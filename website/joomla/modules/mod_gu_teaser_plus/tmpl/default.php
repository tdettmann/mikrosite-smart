<?php
/**
 * Template für Joomla-Modul GU Teaser
 * 
 * @version     1.0.1
 * @package     Joomla.Site
 * @subpackage  gu_teaser_plus
 * @author      Friedrich Schroedter (FS)
 * @copyright   Copyright (C) 2013 GU GROUP Communications, Consulting & Technologies, Karlsruhe.
 * 
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<div class="col-sm-12 teaser-plus<?php echo $params->get('moduleclass_sfx'); ?>">
	<div class="row">
		<?php if ($params->get('image')) : ?>
			<div class="teaser-image grau-hell col-sm-12" <?php if ($params->get('fullwidth')) { echo ' style="background: url(/' . $params->get('image') . ')"'; } ?>>
				<?php if (!$params->get('fullwidth')) : ?>
				<div class="container">
					<div class="row">
						<div class="teaser-image-background">
							<img src="<?php echo $params->get('image'); ?>">
						</div>
					</div>
				</div>
				<?php endif; ?>
			</div>
		<?php endif; ?>
		<div class="teaser-text col-sm-12">
			<div class="container">
				<div class="row">
					<div class="teaser-text-inner col-sm-6 col-sm-offset-6 col-md-7 col-md-offset-5">
						<?php echo $params->get('text'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>