<?php
/**
 * Joomla-Modul GU Teaser PLUS

 * 
 * @version	1.0.1
 * @package	Joomla.Site
 * @subpackage	gu_teaser_plus
 * @author      Friedrich Schroedter (FS)
 * @copyright	Copyright (C) 2013 GU GROUP Communications, Consulting & Technologies, Karlsruhe.
 * 
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
// include helper
//require_once( dirname(__FILE__).'/helper.php' );

// prepare module content
$moduleclass_sfx=$params->get('moduleclass_sfx');

$params->set('module_id',$module->id);

//$modulcontents = modGuTeaserPlusHelper::getModulContent($params,$module,$attribs);

// output modul content
//if (($attribs['ismobile']==1) & !$params->get('mobile'))
require JModuleHelper::getLayoutPath('mod_gu_teaser_plus');