<?php
/**
 * Template für Joomla-Modul GU Slider
 * 
 * @version     1.0.1
 * @package     Joomla.Site
 * @subpackage  gu_slider_plus
 * @author      Friedrich Schroedter (FS)
 * @copyright	Copyright (C) 2015 GU GROUP Communications, Consulting & Technologies, Karlsruhe
 * 
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>

<?php if ($params->get('image')) : ?>
<?php 
	$doc = JFactory::getDocument();
	$image_file = $params->get('image');

	/**
	 *	Desktop image 
	 */
	$style = 
		'.slide-image' . $module->id . ' .image{' .
			'background-image: url(' . $image_file . '); ' .
		'}';
		
	/**
	 *	Mobile image (display >768px)
	 */
	
	$res = 768;
	$image_name = JFile::stripExt($image_file);
	$image_ext =  JFile::getExt($image_file);	
	$image_file_low =  $image_name . '@' . '768' . '.' . $image_ext;
		
	if (JFile::exists(JPATH_ROOT . '/' . $image_file_low)) 
	{
		
		$style .= 
			'@media (max-width: 767px) {' .
				'.slide-image' . $module->id . ' .image{' .
					'background-image: url(' . $image_file_low . '); ' .
				'}'.
			'}';
				
	}
	
	// Add css style declaration
	$doc->addStyleDeclaration( $style );
?>
<div class="slide slide-image<?php echo $module->id; ?>">
	<div class="image"></div>
	<?php if ($params->get('text')) : ?>
		<div class="text">
		<h4><?php echo $params->get('text'); ?></h4>
		<?php if ($params->get('url')) : ?>
			<a  class="readmore" href="<?php echo $params->get('url'); ?>">mehr</a>
		<?php endif; ?>
		</div>
	<?php endif; ?>
</div>	
<?php endif; ?>
