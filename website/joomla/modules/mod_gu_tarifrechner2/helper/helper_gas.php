<?php
/* 
 * Template für Joomla-Modul GU Tarifrechner
 * Tarif-Helper: Tarif 2
 * 
 * @version	    1.0.2
 * @package	    Joomla.Site
 * @subpackage	mod_gu_tarifrechner
 * @author      Friedrich Schroedter
 * @copyright	Copyright (C) 2014 GU GROUP Communications, Consulting & Technologies, Karlsruhe.
 */

defined('_JEXEC') or die('Restricted access');

require_once __DIR__ . '/helper_basic_database.php';

class modGuTarifrechnerTarifGas extends modGuTarifrechnerTarifHelperBasicDatabase
{
	
	private $dbTable = '#__tarif_gas';

	protected function getQuery($db)
	{
		$query = $db->getQuery(true);

		$query->select('*, ((`vertrieb_von`<=NOW()) and (`vertrieb_bis`>=NOW())) as tarifaktuell');
		$query->from($db->quoteName($this->dbTable));
		$query->where($db->quoteName('plz') . '=' . $db->quote($this->formData['tarifplz']));
		$query->where($db->quoteName('verbrauch_von') . "<=" . $db->quote($this->formData['verbrauch']));
		$query->where($db->quoteName('verbrauch_bis') . ">=" . $db->quote($this->formData['verbrauch']));
		$query->where($db->quoteName('laufzeit_von') . "<=" . $db->quote($this->formData['laufzeit']));
		$query->where($db->quoteName('laufzeit_bis') . ">=" . $db->quote($this->formData['laufzeit']));
		
		return $query;
	}

}
