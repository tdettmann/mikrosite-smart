<?php
/**
 * Helper für Joomla-Modul GU Teaser
 * 
 * @version	    1.0.2
 * @package	    Joomla.Site
 * @subpackage	gu_tarifrechner2
 * @author      Friedrich Schroedter
 * @copyright	Copyright (C) 2014 GU GROUP Communications, Consulting & Technologies, Karlsruhe.
 * @todo        Code vollständig dokumentieren
 * 
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

/**
 * 
 */
abstract class modGuTarifrechnerTarifHelperBasic 
{

	protected $tarif;
	
	protected $modulParams;
	
	// Contains all form data
	protected $formData;
	
	
	var $energieart = 1;
	var $requestfields = array(
		'energieart', 'tarifplz', 'verbrauch', 'kundengruppe', 'laufzeit', 'plz', 'anrede',
		'nachname', 'vorname', 'strasse', 'plz', 'ort', 'telefon', 'telefax', 'email', 'firma',
		'kontaktaufnahme', 'vertrags_unterlagen', 'tarifname', 'tarifname2', 'netzanbieter', 'tarifgp', 'tarifap'
	);

	/**
	 *  Contructor der Klasse
	 */
	function __construct($modulParams, $tarif = 0)
	{
		$this->modulParams = $modulParams;
		$this->tarif = $tarif;
	}
	
	// TODO: make an abstract methode
	abstract public function getItems();
	
	/**
	 * 
	 * @return boolean
	 */
	function checkTarifFormDaten()
	{
		$ret = true;
		return $ret;
	}
	
	/**
	 * 
	 * @return boolean
	 */
	function checkKontaktFormDaten()
	{
		$ret = true;
		return $ret;
	}
	
	/**
	 * 
	 * @param type $tarif
	 * @param type $formParams
	 * @return int
	 */
	function getGrundpreisMonat($tarif,&$formParams)
	{
		$gpmonat = 0;
		
		return $gpmonat;
	}
	
	/**
	 * 
	 * @param type $tarif
	 * @param type $formParams
	 * @return int
	 */
	function getArbeitspreisMonat($tarif,&$formParams)
	{
		$ap = 0;
		
		return $ap;
	}
	
	/**
	 * 
	 * @param type $tarif
	 * @param type $formParams
	 * @return int
	 */
	function getPreisMonat($tarif,&$formParams)
	{
		$mp = 0;
		
		return $mp;
	}
	/**
	 * 
	 * @return string
	 */
	function getTarifDbTablename()
	{
		return $this->dbTableTarif;
	}
	
	function getPostcodeFieldname() 
	{
		return 'plz';
	}

	function calcTarifDaten($tarif,&$formParams)
	{
		$tarifDaten=array();
		// Hier werden aus den Datenbank Feldern Tarif abhänige Texte generiert
		return $tarifDaten;
	}

	// Check Items
	function checkItems($items) 
	{
		
		if (empty($items)) 
		{
			if ($items!==false)
			{
				return array(
					'msg' => JText::_('MOD_GU_TARIFRECHNER_ERROR_POSTCODE_NOPROVIDE'),
					'code' => 1
				);
			}
			else
			{
				return array(
					'msg' => JTEXT::_('MOD_GU_TARIFRECHNER_ERROR_DATABASE'),
					'code' => 3
				);
			}	
		} 
		else 
		{
			$tarifeaktuell = false;
			foreach ($items as $key => $item) 
			{
				if ($item['tarifaktuell']==1) 
				{
					// Mindestens einen gültigen Tarif gefunden
					$tarifeaktuell = true;
					break;
				}
			}
			
			// Not even one valid tarif item
			if (!$tarifeaktuell) 
			{
				return array(
					'msg' => JText::_('MOD_GU_TARIFRECHNER_ERROR_TARIFABGELAUFEN'),
					'code' => 4
				);					
				
			}
			
			return array('code' => 0);
		}
		
		
		
	}
	
	function getModulTarifParams($field) {
		return $this->modulParams[$field . $this->tarif];
	}
	
	function checkFormData() 
	{
		// Check Verbrauch
		if ($formData['verbrauch'] > $this->getModulTarifParams('verbrauch'))
		{
			return array(
				'status' => 0,
				'msg' => JTEXT::_('MOD_GU_TARIFRECHNER_ERROR_VERBRAUCH_ZU_HOCH'),
				'code' => 1
			);			
		} 
		// Check PLZ
		elseif (strlen($this->formData['tarifplz']) != 5) // Postleitzahl ok
		{ 
			return array(
				'status' => 0,
				'msg' => JTEXT::_('MOD_GU_TARIFRECHNER_ERROR_POSTCODE_INVALID'),
				'code' => 1
			);	
		}
		
		return array('code' => 0);
	}
	
	function getmodulParams()
	{
		return $this->modulParams;
	}
	
	
	function loadFormData($modulParams)
	{
		// Read POST data
		$input = JFactory::getApplication()->input;
		
		//$modulParams = $this->modulParams;
		
		$this->formData['activetarif']			= $input->get('activetarif',0,'UINT');
		$this->formData['action']				= $input->get('action');
		
		// Verbrauchsdaten
		$this->formData['energieart']			= self::limitRange(abs(intval($input->get('energieart'))),1,2);

		$this->formData['tarifplz']				= $this->cleanPostcode($input->get('tarifplz', '', 'STRING'));
		$this->formData['verbrauch']				= self::limitRange(abs(intval($input->get('verbrauch', 0))), 1, 10000000);
		$this->formData['kundengruppe']			= self::limitRange(abs(intval($input->get('kundengruppe', 0))), 0, 3);
		$this->formData['laufzeit']				= self::limitRange(
														abs(intval($input->get('laufzeit', 0))), 
														$modulParams->get('minlaufzeit', 6), 
														$modulParams->get('maxlaufzeit', 36));

		// Kontaktformular
		$this->formData['anrede']				= self::limitRange(abs(intval($input->get('anrede', 0))), 0, 2);
		$this->formData['nachname']				= $input->get('nachname', '', 'STRING');
		$this->formData['vorname']				= $input->get('vorname', '', 'STRING');
		$this->formData['strasse']				= $input->get('strasse', '', 'STRING');
		$this->formData['plz']					= $this->cleanPostcode($input->get('plz', '', 'STRING'));
		$this->formData['ort']					= $input->get('ort', '', 'STRING');
		$this->formData['telefon']				= $input->get('telefon', '', 'STRING');
		$this->formData['telefax']				= $input->get('telefax', '', 'STRING');
		$this->formData['email']				= $input->get('email', '', 'STRING');
		$this->formData['firma']				= $input->get('firma', '', 'STRING');
		$this->formData['kontaktaufnahme']		= $input->get('kontaktaufnahme', '', 'STRING');
		$this->formData['vertrags_unterlagen']	= $input->get('vertrags_unterlagen', '', 'STRING');

		// Tarifinformationen
		$this->formData['tarifname']			= $input->get('tarifname', '', 'STRING');
		$this->formData['tarifname2']			= $input->get('tarifname2', '', 'STRING');
		$this->formData['netzanbieter']			= $input->get('netzanbieter', '', 'STRING');
		$this->formData['tarifort']				= $input->get('tarifort', '', 'STRING');
		$this->formData['tarifgp']				= $input->get('tarifgp', '', 'STRING');
		$this->formData['tarifap']				= $input->get('tarifap', '', 'STRING');
		return $formData;
	}

	function getFormData() 
	{
		return $this->formData;
	}
	
	function addFormData($key, $value) 
	{
		$this->formData[$key] = $value;
	}
	
	/**
	 * 
	 * @param type $postcode
	 * @return type
	 */
	function cleanPostcode($postcode) 
	{
		//return preg_replace("/[^0-9a-zA-Z]/","",$postcode);
		return preg_replace("/[^0-9]/", "", $postcode);
	}
	
	/**
	 * Prüft ob der übergebene Integer innerhalb des Bereichs min max ist und
	 * setzt ihn gegebenenfals auf den Max oder Min Wert.
	 * @param integer $num
	 * @param integer $min
	 * @param integer $max
	 * @return integer
	 */
	function limitRange($num, $min = 0, $max = PHP_INT_MAX) 
	{
		if ($num < $min) 
		{
			$num = $min;
		}
		elseif ($num > $max) 
		{
			$num = $max;
		}
		
		return $num;
	}
	
	/**
	 * Prüft ob der übergebene Integer innerhalb des Bereichs min max ist.
	 * @param integer $num
	 * @param integer $min
	 * @param integer $max
	 * @return boolean True wenn min <= num <= max sonst falls
	 */
	function checkRange($num, $min = 0, $max = PHP_INT_MAX) 
	{
		if ($num < $min) 
		{
			return false;
		} 
		elseif ($num > $max) 
		{
			return false;
		}
		else 
		{
			return true;
		}
	}
}
