<?php
/**
 * Helper für Joomla-Modul GU Teaser
 * 
 * @version	    1.0.2
 * @package	    Joomla.Site
 * @subpackage	gu_tarifrechner
 * @author      Friedrich Schroedter
 * @copyright	Copyright (C) 2014 GU GROUP Communications, Consulting & Technologies, Karlsruhe.
 * @todo        Code vollständig dokumentieren
 * 
 */
// no direct access
defined('_JEXEC') or die('Restricted access');

require_once __DIR__ . '/helper_basic.php';

abstract class modGuTarifrechnerTarifHelperBasicDatabase extends modGuTarifrechnerTarifHelperBasic 
{

	public function getItems()
	{
		$db = JFactory::getDbo();
		$query = $this->getQuery($db);
		
		try {
			
			$db->setQuery($query);
			$results = $db->loadAssocList();
		} 
		catch (RuntimeException $e) 
		{
			return false; // Database error			
		}
		
		return $results;
	}
	
	abstract protected function getQuery($db);
	
}
