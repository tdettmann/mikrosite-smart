<?php
/* 
 * Template für Joomla-Modul GU Tarifrechner
 * Tarif-Helper: Tarif 1
 * 
 * @version	    1.0.2
 * @package	    Joomla.Site
 * @subpackage	mod_gu_tarifrechner
 * @author      Friedrich Schroedter
 * @copyright	Copyright (C) 2014 GU GROUP Communications, Consulting & Technologies, Karlsruhe.
 */

defined('_JEXEC') or die('Restricted access');

require_once __DIR__ . '/helper_basic.php';

class SoapParams
{
	public $strUser;
	public $strPasswort;
	public $strParameter;
	
	function __construct($user, $passwort, $parameter) 
	{
		$this->strUser = $user;
		$this->strPasswort = $passwort;
		$this->strParameter = $parameter;
	}
}

class modGuTarifrechnerTarifStromWebservice extends modGuTarifrechnerTarifHelperBasic
{
	private $url = 'https://www.swp-vertrieb.de/partnerService/partnerService.svc';
	
	private $user = "gukWebserviceUser";
	
	private $password = "XASjtqY79Y";
	
	public function getItems()
	{
		try 
		{
			$client = new SoapClient($this->url . '?wsdl',
			array(	'compression'	=> SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP,
					//'cache_wsdl'	=> WSDL_CACHE_BOTH,
					//'login'			=> "gukWebserviceUser", 
					//'password'		=> "XASjtqY79Y",
					//'exception'		=> true,
					//'authentication' => SOAP_ATHENTICATION_DIGEST,
					'trace'			=> true
			));
		} 
		catch(SoarpFault $e)
		{
			// var_dump($e);
			return false; // Webservice error
		}
		
		return $this->requestSOAPService($client);
	}

	private function requestSOAPService( $client ) 
	{
		$param = new SoapParams( 	$this->user, 
									$this->password, 
									"PLZ="			. $this->formData['tarifplz']	. ";".
									"Verbrauch="	. $this->formData['verbrauch']	. ";".
									"Laufzeit="		. $this->formData['laufzeit']	. ";".
									"ProjektID=100004" );
									
		// Get soap response
		$csv = $client->GetTarifCsv($param);
		
		// "GetTarifCsvResult" is a soap object
		$items = $this->csvToArray( $csv->GetTarifCsvResult );
		
		return $items;
	}
	
	private function csvToArray( $csv ) 
	{
		//print_r( $csv );
		//echo "------------";
		
		$items = array();
		
		// String stream of csv data to handle very big data tables
		$handle = fopen('php://memory','r+');
		fwrite($handle, $csv);
		rewind($handle);

		// Get tarif header
		if (!feof($handle)) 
		{
			$header_csv = stream_get_line($handle, 1000, "\r\n");
			$header 	= str_getcsv($header_csv, ";");
		} 
		
		// Get tarif data
		while (!feof($handle)) {
			$line = stream_get_line($handle, 1000, "\r\n");
			$item = str_getcsv($line, ";");
			$item = array_combine($header, $item);
			
			$item = $this->transformResults( $item );
			
			array_push($items, $item);
		}
		
		return $items;
	}
	
	private function intDate($date) {
		return strtotime(date('Y-m-d 00:00:00', strtotime($date)));
	}
	
	private function transformResults($item) 
	{
		// Reduce keys, convert values, convert keys, create keys
		$transform = array();
		
		date_default_timezone_set('Europe/Berlin');
		$tarifaktuell = ((($this->intDate($item['vertrieb_von']) <= $this->intDate("now")) &&
						  ($this->intDate($item['vertrieb_bis']) >= $this->intDate("now"))) ? 1 : 0 );
		
		$transform['plz']							= $item['PLZ'];
		$transform['verbrauch_von']					= $item['swp_Verbrauch_von'];
		$transform['verbrauch_bis']					= $item['swp_Verbrauch_bis'];
		$transform['laufzeit_von']					= $item['LaufzeitMonate_von'];
		$transform['laufzeit_bis']					= $item['LaufzeitMonate_bis'];
		$transform['gp_brutto_monat']				= floatval($item['GP_Brutto']) / 12;	// Calculate price per month
		$transform['ap_brutto']						= $item['AP_Brutto'];
		$transform['tarifaktuell']					= $tarifaktuell;
		
		$transform['tarifort']						= $item['Orte'];
		$transform['netzanbieter']					= $item['Netzbetreiber'];
		
		$transform['GKZ8']							= $item['GKZ8'];
		$transform['Netzbetreiber_Verbandsnummer']	= $item['Netzbetreiber_Verbandsnummer'];
		
		return $transform;
	}
	
}
