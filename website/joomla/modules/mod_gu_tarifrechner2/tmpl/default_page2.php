<?php
/**
 * Template für Joomla-Modul GU Tarifrechner
 * Schritt / Page 2: Tarif anzeigen
 * 
 * @version	    1.0.2
 * @package	    Joomla.Site
 * @subpackage	mod_gu_tarifrechner
 * @author      Thomas Langer (TL)
 * @copyright	Copyright (C) 2014 GU GROUP Communications, Consulting & Technologies, Karlsruhe.
 * 
 */
// no direct access
defined('_JEXEC') or die('Restricted access');
?>
<div class="page2">
	<div class="kontaktcol">
		<div class="gu_tarifangebot gu_tarifmsg">&nbsp;</div>
		<div class="gu_tarifdetails gu_tarifmsg">&nbsp;</div>
	</div>
	<div class="formcol3">
		<form class="gu_tarifanfordern">
			<input type="button" 
				   class="gu_tarif_button_anpassen gu_tarif_button submitbutton" 
				   value="<?php echo JText::_('MOD_GU_TARIFRECHNER_FORMSTART_BUTTON'); ?>" 
			/>
			<div class="gu_tarifspacer"></div>
			<input name="tarifanfordern" 
				   type="button" 
				   class="gu_tarif_button_zumformular gu_tarif_button submitbutton" 
				   value="<?php echo $params->get('buttomzumkontaktformular',JText::_('MOD_GU_TARIFRECHNER_VIEWFORM_BUTTON')); ?>" 
			/>
		</form>
	</div>
</div>