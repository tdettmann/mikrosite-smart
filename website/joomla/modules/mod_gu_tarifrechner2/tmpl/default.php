<?php
/**
 * Template für Joomla-Modul GU Tarifrechner
 * 
 * @version	1.0.2
 * @package	Joomla.Site
 * @subpackage	mod_gu_tarifrechner
 * @author      Thomas Langer (TL)
 * @copyright	Copyright (C) 2014 GU GROUP Communications, Consulting & Technologies, Karlsruhe.
 * @todo        Code vollständig dokumentieren
 * 
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

if (($params->get('online',0)==1) and (($params->get('tarif1online',0)==1) or ($params->get('tarif2online',0)==1))):
//if ($params->get('online',0)==1):
?>
<div class="gu_tarifrechner<?php if ($params->get('showtariftap',0)==1){ echo ' tariftabrowvisible';} ?>">
	<div class="gu_tarifrechner_loading" style="display:none;"><img src="<?php echo JURI::base(); ?>images/layout/ajax-loader.gif"/>
	</div>
	
	<?php if ($params->get('showtariftap',0)): ?>
		<div class="tariftabrow" style="width:auto;">
			<?php if ($params->get('tarif1online',0)): ?>
				<div class="tabtarif1 tabtarif<?php if ($params->get('activetarif',1)==1) { echo ' tabaktiv'; } ?>" data-tariftype="1">
					<a href="#"><?php 
						if ($params->get('tarifart1')==1)
						{
							echo JText::_('MOD_GU_TARIFRECHNER_ENERGIE_STROM');
						}
						else
						{
							echo JText::_('MOD_GU_TARIFRECHNER_ENERGIE_GAS');
						}
					?></a>
				</div>
			<?php endif; ?>
			<?php if ($params->get('tarif3online',0)): ?>
				<div class="tabtarif3 tabtarif<?php if ($params->get('activetarif',1)==3) { echo ' tabaktiv'; } ?>" data-tariftype="3">
					<a href="#"><?php 
						if ($params->get('tarifart3')==1)
						{
							echo JText::_('MOD_GU_TARIFRECHNER_ENERGIE_STROM');
						}
						else
						{
							echo JText::_('MOD_GU_TARIFRECHNER_ENERGIE_GAS');
						}
					?></a>
				</div>
			<?php endif; ?>
			<?php if ($params->get('tarif2online',0)): ?>
				<div class="tabtarif2 tabtarif<?php if ($params->get('activetarif',1)==2) { echo ' tabaktiv'; } ?>" data-tariftype="2">
					<a href="#"><?php 
						if ($params->get('tarifart2')==1)
						{
							echo JText::_('MOD_GU_TARIFRECHNER_ENERGIE_STROM');
						}
						else
						{
							echo JText::_('MOD_GU_TARIFRECHNER_ENERGIE_GAS');
						}
					?></a>
				</div>
			<?php endif; ?>
		</div>
	<?php endif; ?>
	<div class="gu_tarifrechner_col1">
		<h3 class="gu_tarifrechner_claim"><?php
			$aktivtarif = $params->get('activetarif',1);
			echo $params->get('claim' . $aktivtarif );
		?></h3>
	</div>
	<div class="gu_tarifrechner_inner">
		<?php 
		require_once( dirname(__FILE__).'/default_page1.php' ); 
		require_once( dirname(__FILE__).'/default_page1_details.php' );
		require_once( dirname(__FILE__).'/default_page2.php' ); 
		require_once( dirname(__FILE__).'/default_page3.php' ); 
		require_once( dirname(__FILE__).'/default_page4.php' );
		// require_once( dirname(__FILE__).'/default_page5.php' );
		?>
	</div>
	<div class="clear"></div>
</div>
<script type="text/javascript">
	var guTarifrechner2 = new GUTarifRechner('',mod_gutarifrechner_tarifdaten);
</script>
<?php 
else:
	// Offline
	if ($params->get('offlinecolor'))
	{
		$style.='background-color: '.$params->get('offlinecolor').'; ';
	}
	if ($params->get('offlineimage'))
	{
		$style.='background-image: url('.$params->get('offlineimage').');';
	}
	$style=' style="'.$style.'"';
?>
<div class="gu_tarifrechner gu_tarifrechner_offline"<?php echo $style; ?>>
	<h3><?php echo $params->get('offlineclaim'); ?></h3>
	<div class="offlinetext"><?php echo $params->get('offlinetext','');?></div>
	<div class="clear"></div>
</div>
<?php endif;