<?php
/**
 * Template für Joomla-Modul GU Tarifrechner
 * Schritt / Page 1: Tarifformular
 * 
 * @version	   1.0.2
 * @package	    Joomla.Site
 * @subpackage	mod_gu_tarifrechner
 * @author      Friedrich Schroedter
 * @copyright	Copyright (C) 2014 GU GROUP Communications, Consulting & Technologies, Karlsruhe.
 * 
 */
// no direct access
defined('_JEXEC') or die('Restricted access');
?>
<div class="page6">
		<div class="tarif3">
			<form class="gu_tarifform_details" autocomplete="off">
				<div id="form2col" style="">
					<div class="gu_tarif_formmsg"></div>
					<div class="gu_tarifspacer"></div>
					
					<label class="labeltarif_ort_netz">Ort / Netzbetreiber</label>
					<select size="1" name="tarif_ort_netz" data-placeholder="Bitte wählen Sie Ihren Ort und Netzbetreiber..."></select>
				</div>
				<div class="clear"></div>
			</form>
		
			<div class="formcol3">
				<div>
					<input type="button" 
					   class="gu_tarif_button_anpassen gu_tarif_button submitbutton" 
					   value="<?php echo JText::_('MOD_GU_TARIFRECHNER_FORMSTART_BUTTON'); ?>" 
					/>
					<div class="gu_tarifspacer"></div>
					<input name="tarifanfordern" 
					   type="button" 
					   class="gu_tarif_button_zumangebot gu_tarif_button submitbutton" 
					   value="Zum Angebot" 
					/>
					<!-- <div class="gu_tarifspacer"></div> -->
					<!--div class="gu_tarif_formmsg"></div-->
				</div>
			</div>
			<div class="clear"></div>
		</div>
</div>