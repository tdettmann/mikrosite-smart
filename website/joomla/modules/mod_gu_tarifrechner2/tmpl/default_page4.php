<?php
/** 
 * Template für Joomla-Modul GU Tarifrechner
 * Schritt / Page 4: Danke Seite
 * 
 * @version	    1.0.2
 * @package	    Joomla.Site
 * @subpackage	mod_gu_tarifrechner
 * @author      Thomas Langer (TL)
 * @copyright	Copyright (C) 2014 GU GROUP Communications, Consulting & Technologies, Karlsruhe.
 * 
 */
// no direct access
defined('_JEXEC') or die('Restricted access');
?>
<div class="page4">
	<div class="form2col gu_tarif_msg">&nbsp;</div>
	<div class="formcol3">
		<form>
			<input type="button" 
				   class="gu_tarif_button_neueanfrage gu_tarif_button submitbutton" 
				   value="<?php echo JText::_('MOD_GU_TARIFRECHNER_FORMNEUSTART_BUTTON'); ?>" 
			/>
		</form>
	</div>
</div>