<?php
/**
 * Template für Joomla-Modul GU Tarifrechner
 * Schritt / Page 5: Nicht verwendet.
 * 
 * @version	    1.0.2
 * @package	    Joomla.Site
 * @subpackage	mod_gu_tarifrechner
 * @author      Thomas Langer (TL)
 * @copyright	Copyright (C) 2014 GU GROUP Communications, Consulting & Technologies, Karlsruhe.
 * 
 */
// no direct access
defined('_JEXEC') or die('Restricted access');
?>
<div class="page5">
	<div class="gu_tarifmsg">&nbsp;</div>
</div>