<?php
/**
 * Helper für Joomla-Modul GU Teaser
 * 
 * @version	    1.0.2
 * @package	    Joomla.Site
 * @subpackage	gu_tarifrechner
 * @author      Friedrich Schroedter
 * @copyright	Copyright (C) 2014 GU GROUP Communications, Consulting & Technologies, Karlsruhe.
 */

 // no direct access
defined('_JEXEC') or die('Restricted access');

require_once __DIR__ . '/helper/helper_basic.php';

class modGuTarifrechner2Helper
{

	protected static $dbTableLog = '#__tarif_log';
	protected static $modulParams = null;
	protected static $tarifHelper = Null;

	/**
	 * method=[custom fragment]
	 * @return type
	 */

	// Send form via email
	public static function sendAnfrageAjax() {

		
		//echo "***";
		
		// Modulparameter laden // self::$modulParams
		self::loadModulParams();
		
		//echo "***";

		// Tarifhelper + Formulardaten laden // self::$tarifhelper
		self::getTarifHelper();
		
		
		
		$formData = self::$tarifHelper->getFormData();
		
		
		
		
		$fehler = 0;
		$app = JFactory::getApplication();
		// http://docs.joomla.org/Sending_email_from_extensions
		$mailer = JFactory::getMailer();
		$result = array(
			'status' => 0,
			'msg' => '',
			'error' => -1
		);
		
		
		
		$errorfields = array();
		// Formularprüfung
		// Pflichtfelder sind Anrede Name und E-Mail-Adresse
		// E-Mail prüfen
		if (!$mailer->ValidateAddress($formData['email'])) {
			$fehler = 1;
			if ($result['msg'] != '') {
				$result['msg'].='<br />';
			}
			$errorfields[]='email';
			$result['msg'].='Bitte geben Sie Ihre E-Mail-Adresse an.';
		}

		if ($formData['anrede'] == 0) {
			$fehler = 1;
			if ($result['msg'] != '') {
				$result['msg'].='<br />';
			}
			$errorfields[]='anrede';
			$result['msg'].='Bitte wählen Sie Ihre Anrede aus. ';
		}

		if ($formData['nachname'] == '') {
			$fehler = 1;
			if ($result['msg'] != '') {
				$result['msg'].='<br />';
			}
			$errorfields[]='nachname';
			$result['msg'].='Bitte geben Sie Ihren Nachnamen an. ';
		}
		
//		if (self::$formParams['firma'] == '') {
//			$fehler = 1;
//			if ($result['msg'] != '') {
//				$result['msg'].='<br />';
//			}
//			$errorfields[]='firma';
//			$result['msg'].='Bitte geben Sie Ihren Firmennamen an. ';
//		}
//		if (self::$formParams['telefon'] == '') {
//			$fehler = 1;
//			if ($result['msg'] != '') {
//				$result['msg'].='<br />';
//			}
//			$errorfields[]='telefon';
//			$result['msg'].='Bitte geben Sie Ihre Telefonnumer an. ';
//		}

		// Keine Pflichtfeld leer
		if ($fehler == 0) {
			// E-Mail Einstellungen holen
			$sendermail = self::$modulParams->get('sendermail', '');
			$sendername = self::$modulParams->get('sendername', '');
			$customersubject = self::$modulParams->get('customersubject', 'Anfrage');
			$adminsubject = self::$modulParams->get('adminsubject', 'Anfrage');
			$customerbody = self::makeCustomText(self::$modulParams->get('customertext', JText::_('MOD_GU_TARIFRECHNER_DANKETEXT')), $formData);
			$adminbody    = self::makeCustomText(self::$modulParams->get('admintext', JText::_('MOD_GU_TARIFRECHNER_DANKETEXT')), $formData);
			// Absender und Absendername nicht im Modul gesetzt
			if ($sendermail == '') {
				$sendermail = $app->getCfg('mailfrom');
			}
			if ($sendername == '') {
				$sendermail = $app->getCfg('fromname');
			}

			// Joomla Mailer
			$sender = array($sendermail, $sendername);

			// Auskommentieren wenn HTML E-Mails versendet werden
			// Siehe http://docs.joomla.org/Sending_email_from_extensions
//			$mailer->isHTML(true);
//			$mailer->Encoding = 'base64';
			// Mail an Kunde
			$mailer->setSender($sender);
			$mailer->addRecipient($formData['email']);
			$mailer->setSubject($customersubject);
			
			$mailer->setBody($customerbody);
			$send = $mailer->Send();
			if ($send !== true) {
//			echo 'Error sending email: ' . $send->__toString();
			} else {
//			echo 'Mail sent';
			}

			// Mail an Admin / Betreiber
			// Neuer Mailer
			unset($mailer);
			$mailer = JFactory::getMailer();
			$adminmail = self::$modulParams->get('adminmail', '');

			$mailer->setSender($sender);
			$mailer->addRecipient($adminmail);
			$mailer->setSubject($adminsubject);
			$mailer->setBody($adminbody);
			$send = $mailer->Send();
			if ($send !== true) {
//				echo 'Error sending email: ' . $send->__toString();
			} else {
//				echo 'Mail sent';
			}

			// JSON Inhalte
			$result['danketext'] = self::makeCustomText(self::$modulParams->get('danketext', JText::_('MOD_GU_TARIFRECHNER_DANKETEXT')), $formData);
			$result['status'] = 1;
			$result['msg'] = 'OK';
			// E-Mails in der Datenbank speichern
			if (self::$dbTableLog!=='')
			{
				try {
					$logdb = JFactory::getDbo();
					$logquery = $logdb->getQuery(true);
					
					$columns = array('sendtime', 'email', 'admin_email', 'mailtext', 'mailtext_admin');

					$values = array(
						$logdb->quote(date("Y-m-d H:i:s")), 
						$logdb->quote($formData['email']), 
						$logdb->quote($adminmail),
						$logdb->quote($customersubject."\n\n".$customerbody),
						$logdb->quote($adminsubject."\n\n".$adminbody)
					);

					$logquery->insert($logdb->quoteName(self::$dbTableLog));
					$logquery->columns($logdb->quoteName($columns));
					$logquery->values(implode(',', $values));
					//echo $logquery;
					$logdb->setQuery($logquery);
					$logdb->query();
				} catch (RuntimeException $e) {
				}
			}	
		}
		else
		{
			$result['errorfields']=$errorfields;
		}
		
		
		
		// Ergebniss zurück an Browser
		return $result;
	}


	/**
	 * 
	 * @return type
	 */
	protected static function makeCustomText($text, $formData, $platzhalter=0) {
		$placeholder = array();
		if (is_array($platzhalter))
		{
			$placeholder = $platzhalter;
		}
		$placeholder['datum'] = date(JText::_('MOD_GU_TARIFRECHNER_FORMAT_DATUM')); //'d.m.Y'
		$placeholder['datumuhr'] = date(JText::_('MOD_GU_TARIFRECHNER_FORMAT_DATUM_UHR')); // 'd.m.Y H:i:s'
		switch ($formData['energieart']) {
			case 1: 
				$placeholder['tarifart'] = JText::_('MOD_GU_TARIFRECHNER_ENERGIE_STROM');
				$placeholder['energieart'] = JText::_('MOD_GU_TARIFRECHNER_ENERGIE_STROM');
				$placeholder['energie'] = JText::_('MOD_GU_TARIFRECHNER_ENERGIE_STROM');
				break;
			case 2: 
				$placeholder['tarifart'] = JText::_('MOD_GU_TARIFRECHNER_ENERGIE_GAS');
				$placeholder['energieart'] = JText::_('MOD_GU_TARIFRECHNER_ENERGIE_GAS');
				$placeholder['energie'] = JText::_('MOD_GU_TARIFRECHNER_ENERGIE_GAS');
				break;
		}
		switch ($formData['kundengruppe']) {
			case 1: $placeholder['kunde'] = JTEXT::_('MOD_GU_TARIFRECHNER_PRIVATKUNDE');//'Privatkunde';
				break;
			case 2: $placeholder['kunde'] = JTEXT::_('MOD_GU_TARIFRECHNER_GEWERBEKUNDE');//'Gewerbekunde';
				break;
			default: $placeholder['kunde'] = JTEXT::_('MOD_GU_TARIFRECHNER_PRIVAT_UND_GEWERBE');//'Privatkunde';
		}
		switch ($formData['anrede']) {
			case 2: $placeholder['anrede'] = JTEXT::_('MOD_GU_TARIFRECHNER_ANREDE_HERR');//'Herr';
				$placeholder['anredeformal'] = JTEXT::_('MOD_GU_TARIFRECHNER_ANREDE_MAIL_HERR');//'Sehr geehrter Herr';
				break;
			case 1: $placeholder['anrede'] = JTEXT::_('MOD_GU_TARIFRECHNER_ANREDE_FRAU');//'Frau';
				$placeholder['anredeformal'] = JTEXT::_('MOD_GU_TARIFRECHNER_ANREDE_MAIL_FRAU');//'Sehr geehrte Frau';
				break;
			default: $placeholder['anrede'] = '';
				$placeholder['anredeformal'] = JTEXT::_('MOD_GU_TARIFRECHNER_ANREDE_MAIL_NEUTRAL');//'Hallo';
		}
		return self::makeText($text, $formData, $placeholder);
	}


	/**
	 * Ersetzt Platzhalter in der Form {plazhaltername} im Text durch die Werte.
	 * Der Plazhaltername wird vor der Suche im Text um {} Klammern ergänzt.
	 * @param string $text Text der bearbeitet werden soll
	 * @param array $keyValues array('plazhaltername'=>'Wert')
	 * @return string Fertiger Text
	 */
	public static function makeText($text, $formData, $keyValues) 
	{
		foreach ($keyValues as $key => $value) 
		{
			$text = str_replace('{' . $key . '}', $value, $text);
		}
		
		foreach ($formData as $key => $value) 
		{
			$text = str_replace('{' . $key . '}', $value, $text);
		}
		
		return $text;
	}
	
	/**
	 * Tarif Request
	 * @return array
	 */
	public static function getAjax() 
	{
		// Modulparameter laden // self::$modulParams
		self::loadModulParams();

		// Tarifhelper + Formulardaten laden // self::$tarifhelper
		self::getTarifHelper();
		
		$formData = self::$tarifHelper->getFormData();
		
		// Check input
		$error = self::$tarifHelper->checkFormData();
		
		if ($error['code'] == 0) 
		{
			switch ($formData['action']) 
			{
				case 'sendformular' :
					// Send Formular
					// sendForm();
					break;
					
				case  'tarif':
				default:
				
					// Get tarif input page
					$items = self::$tarifHelper->getItems();
					$error = self::$tarifHelper->checkItems($items);
				
					if ($error['code'] == 0)
					{
						$tarif_ort_netz = array();
						$result['items'] = array();
						
						foreach ($items as $key => $item) 
						{
							if ($item['tarifaktuell'] == 1)
							{ 
								array_push($result['items'], self::calcItem($item, $formData) );
							
								// Only for WebService tarif
								if ($formData['activetarif'] == 3)
								{
									array_push($tarif_ort_netz, $item['tarifort'] . ": " . $item['netzanbieter']);
								}
							}
						}
						
					}
			}
			
		}
		
		
		if (count($result['items']) >  1) 
		{
			$result['tarif_ort_netz'] = JHTML::_('select.options', $tarif_ort_netz);
		}
		
		$result['error'] = $error;
		

		// Geprüfte und gegebenenfals geänderte Formularparmeter zurücksenden
		$result['energieart']	= $formData['energieart'];
		$result['tarifplz']		= $formData['tarifplz'];
		$result['verbrauch']	= $formData['verbrauch'];
		$result['kundengruppe']	= $formData['kundengruppe'];
		$result['laufzeit']		= $formData['laufzeit'];
		

		// Ergebniss zurück an Browser
			
		return $result;
	}

	/**
	 * Führt Berechnungen/Formatierungen an den abgerufenen Datensätzen durch
	 * @param array $item Datensatz
	 * @param array $params Parameter
	 * @return type
	 */
	static function calcItem($item, $formData) 
	{
	
		$activetarif = $formData['activetarif'];

		
		
		switch ($formData['kundengruppe']) 
		{
			case 1: $kunde =  JTEXT::_('MOD_GU_TARIFRECHNER_PRIVATKUNDE');//'Privatkunde';
				break;
			case 2: $kunde =  JTEXT::_('MOD_GU_TARIFRECHNER_GEWERBEKUNDE');//'Gewerbekunde';
				break;
			default: $kunde =  JTEXT::_('MOD_GU_TARIFRECHNER_PRIVAT_UND_GEWERBE');//'Privatkunde';
		}

		$platzhalter = array();
		
		$monatspreis = round(((($item['ap_brutto'] * $formData['verbrauch']) / 100) / 12) + $item['gp_brutto_monat'], 2);

		// Angebotstext erstellen
		
		$platzhalter['monatspreis']		= number_format($monatspreis, 2, ',', '');
		$platzhalter['grundpreis']		= number_format($item['gp_brutto_monat'], 2, ',', '');
		$platzhalter['arbeitsspreis']	= number_format($item['ap_brutto'], 2, ',', '');

		$platzhalter['tarifort']		= $item['tarifort'];
		$platzhalter['netzanbieter']	= $item['netzanbieter'];
		
		$platzhalter['tarifplz']		= $item['plz'];
		$platzhalter['kunde']			= $kunde;
		$platzhalter['laufzeit']		= $formData['laufzeit'];
		$platzhalter['verbrauch']		= $formData['verbrauch'];

		$newitem = array();
		$newitem['angebottext']			= 	self::makeCustomText(
		//self::$modulParams
												self::$modulParams->get('tarifangebottext'.$activetarif,
												JText::_('MOD_GU_TARIFRECHNER_ANGEBOTTEXT')), 
												$formData, 
												$platzhalter
											);

		$newitem['tarifdetails'] 		= self::makeCustomText(self::$modulParams->get('tarifangebotdaten'.$activetarif, 
												JText::_('MOD_GU_TARIFRECHNER_ANGEBOTTEXT2')), 
												$formData, 
												$platzhalter);
										
		$newitem['tarifzusammenfassung'] = self::makeCustomText(self::$modulParams->get('tarifzusammenfassung'.$activetarif, 
											JText::_('MOD_GU_TARIFRECHNER_ANGEBOTTEXT3')), 
											$formData, 
											$platzhalter
										);
										
		$newitem['tarifgp'] = number_format($item['gp_brutto_monat'], 2, ',', '');
		$newitem['tarifap'] = number_format($item['ap_brutto'], 2, ',', '');
		$newitem['netzanbieter'] = $item['netzanbieter'];
		$newitem['tarifort'] = $item['tarifort'];
		$newitem['tarifname'] = $item['tarifname'];
		$newitem['tarifname2'] = $item['tarifnummer'];
		$newitem['tarifname3'] = $item['tarifsubnummer'];
		return $newitem;
	}


	public static function getCustomerSelect($kundengruppe){
		$html='';
		switch (intval($kundengruppe))
		{
			case 0: $html='<div class="textkundengruppe">'.JText::_('MOD_GU_TARIFRECHNER_FUER_PRIVAT_UND_GEWERBE').'</div>'
						.'<input type="hidden" name="kundengruppe" value="0" />';
					break;
			case 1: $html='<div class="textkundengruppe">'.JText::_('MOD_GU_TARIFRECHNER_FUER_PRIVATKUNDEN').'</div>'
						.'<input type="hidden" name="kundengruppe" value="1" />';
					break;
			case 2: $html='<div class="textkundengruppe">'.JText::_('MOD_GU_TARIFRECHNER_FUER_GEWERBEKUNDEN').'</div>'
						.'<input type="hidden" name="kundengruppe" value="2" />';
					break;
			default: $html='<select name="kundengruppe" size="1" data-placeholder="'.JText::_('MOD_GU_TARIFRECHNER_KUNDENGRUPPE').'">'
					  . '<option value="-1" selected="selected" style="display:none;">'.JText::_('MOD_GU_TARIFRECHNER_KUNDENGRUPPE').'</option>'
					  .'<option value="1">'.JText::_('MOD_GU_TARIFRECHNER_PRIVATKUNDE').'</option>'
					  .'<option value="2">'.JText::_('MOD_GU_TARIFRECHNER_GEWERBEKUNDE').'</option>'
					  .'</select>';
					break;
		}
		return $html;
	}

	/**
	 * Speichert und bereinigt die empfangenen Formular Daten
	 */
	public static function getTarifHelper()
	{
		//echo "###";
		
		// Read POST data
		$input = JFactory::getApplication()->input;
		$activetarif = $input->get('activetarif',0,'UINT');
		
		//echo "###1";
		
		// Tarifhelpers laden
		self::setTarifHelper( $activetarif );
		
		//echo "###2";
		
		self::$tarifHelper->loadFormData(self::$modulParams);
		
		//echo "###3";
		
	}
	
	
	/**
	 * Lädt den zur energieart gehörigen Tarif Helper und erzeugt ein Objekt
	 * @param integer $energieart [1: Strom, 2: Gas, null: Fallback]!
	 */
	protected static function setTarifHelper($tarif) 
	{
		//echo "----" . $tarif . "----";
		// Include the helper.
		switch ($tarif) 
		{
			case 1:                     
				require_once __DIR__ . '/helper/helper_strom.php';
				self::$tarifHelper = new modGuTarifrechnerTarifStrom(self::$modulParams, $tarif);
				break;
			case 2:
				//echo "+++";
				require_once __DIR__ . '/helper/helper_gas.php';
				//echo "+++";
				self::$tarifHelper = new modGuTarifrechnerTarifGas(self::$modulParams, $tarif);
				break;
			case 3:
				require_once __DIR__ . '/helper/helper_strom_webservice.php';
				self::$tarifHelper = new modGuTarifrechnerTarifStromWebservice(self::$modulParams, $tarif);
				break;				
				
			default:
				self::$tarifHelper = new modGuTarifrechnerTarifHelperBasic(self::$modulParams);
		}

	}

	/**
	 * Lädt die gespeicherten Modul-Einstellungen und Sprachtexte
	 * @param string $modultitle Optional Titel des Moduls zur besseren identifizierung des Moduls
	 */
	protected function loadModulParams($modultitel = null) {
		// Module Parameter Laden
		jimport('joomla.application.module.helper');
		$module = JModuleHelper::getModule('gu_tarifrechner2', $modultitel);
		self::$modulParams = new JRegistry();
		self::$modulParams->loadString($module->params);
		$lang = JFactory::getLanguage();
		$lang->load('mod_gu_tarifrechner', JPATH_ROOT);
		
	}

}