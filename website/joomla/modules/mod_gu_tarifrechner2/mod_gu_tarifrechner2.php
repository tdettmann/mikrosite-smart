<?php
/**
 * Helper für Joomla-Modul GU Teaser
 * 
 * @version	1.0.1
 * @package	Joomla.Site
 * @subpackage	gu_tarifrechner
 * @author      Friedrich Schroedter
 * @copyright	Copyright (C) 2014 GU GROUP Communications, Consulting & Technologies, Karlsruhe.
 * @todo        Code vollständig dokumentieren
 * 
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 

// Include the helper.
require_once __DIR__ . '/helper.php';

// Tarifrechner online?
if ($params->get('online',0)==1)
{ 	// online -> load js file
	$document = JFactory::getDocument();
	//$modulhelper = modGuTeaserHelper::setModulParams($params);
	//$modulhelper = new modGuTarifrechnerHelper;
	JText::_("MOD_GU_TARIFRECHNER_ERROR_FORM", array("script"=>true));
	JText::_("MOD_GU_TARIFRECHNER_ERROR_TRY_LATER", array("script"=>true));
	JText::_("MOD_GU_TARIFRECHNER_FUER_PRIVAT_UND_GEWERBE", array("script"=>true));
	JText::_("MOD_GU_TARIFRECHNER_FUER_PRIVATKUNDEN", array("script"=>true));
	JText::_("MOD_GU_TARIFRECHNER_FUER_GEWERBEKUNDEN", array("script"=>true));

	// AUfbereitung Tarifdaten für JS
	$jsTarifdaten='';
	// JS Daten für Tarif 1
	if ($params->get('tarif1online',0)==1)
	{
		$jsTarifdaten[]=
			'tarif1:{'
			.'"tarif":1,'
			.'"energieart":'.$params->get('tarifart1').','
			.'"kundengruppe":'.$params->get('kundengruppe1').','
			.'"claim":"'.addslashes($params->get('claim1')).'",'
			.'"minlaufzeit":"'.addslashes($params->get('minlaufzeit1')).'",'
			.'"maxlaufzeit":"'.addslashes($params->get('maxlaufzeit1')).'",'
			.'"minverbrauch":"'.addslashes($params->get('minverbrauch1')).'",'
			.'"maxverbrauch":"'.addslashes($params->get('maxverbrauch1')).'"'
			.'}';
	}
	else 
	{
		$jsTarifdaten[]= '1:{}';
	}
	
	// JS Daten für Tarif 2
	if ($params->get('tarif2online',0)==1)
	{
		$jsTarifdaten[]=
			'tarif2:{'
			.'"tarif":2,'
			.'"energieart":'.$params->get('tarifart2').','
			.'"kundengruppe":'.$params->get('kundengruppe2').','
			.'"claim":"'.addslashes($params->get('claim2')).'",'
			.'"minlaufzeit":"'.addslashes($params->get('minlaufzeit2')).'",'
			.'"maxlaufzeit":"'.addslashes($params->get('maxlaufzeit2')).'",'
			.'"minverbrauch":"'.addslashes($params->get('minverbrauch2')).'",'
			.'"maxverbrauch":"'.addslashes($params->get('maxverbrauch2')).'"'
			.'}';
	}
	else 
	{
		$jsTarifdaten[] .= '2:{}';
	}
	
	// JS Daten für Tarif 3
	if ($params->get('tarif3online',0)==1)
	{
		$jsTarifdaten[]=
			'tarif3:{'
			.'"tarif":3,'
			.'"energieart":'.$params->get('tarifart3').','
			.'"kundengruppe":'.$params->get('kundengruppe3').','
			.'"claim":"'.addslashes($params->get('claim3')).'",'
			.'"minlaufzeit":"'.addslashes($params->get('minlaufzeit3')).'",'
			.'"maxlaufzeit":"'.addslashes($params->get('maxlaufzeit3')).'",'
			.'"minverbrauch":"'.addslashes($params->get('minverbrauch3')).'",'
			.'"maxverbrauch":"'.addslashes($params->get('maxverbrauch3')).'"'
			.'}';
	}
	else 
	{
		$jsTarifdaten[] .= '3:{}';
	}

	$document->addScriptDeclaration('var mod_gutarifrechner_tarifdaten={'.implode(',',$jsTarifdaten).'};');
			
	$document->addScript(JURI::base(true) . '/modules/mod_gu_tarifrechner2/assets/mod_gutarifrechner.js');
}
require JModuleHelper::getLayoutPath('mod_gu_tarifrechner2');