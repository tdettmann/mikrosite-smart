<?php
/**
 * Helper für Joomla-Modul GU Teaser
 * 
 * @version	1.0.1
 * @package	Joomla.Site
 * @subpackage	gu_teaser
 * @author      Friedrich Schroedter (FS), Thomas Langer (TL)
 * @copyright	Copyright (C) 2013 GU GROUP Communications, Consulting & Technologies, Karlsruhe.
 * @todo        Code vollständig dokumentieren
 * 
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' ); 
require_once (JPATH_SITE . '/components/com_content/helpers/route.php'); 

    
class modGuTeaserHelper
{
	/**
	 * erzeugt das Visual Element
	 * @param object $params Modul Parameter Objekt
	 * @return string HTML-Code für das Element
	 */
	static function getvisual($params)
	{
		$html = '';
		if ($params->get('visualview')) {
			$visualstyle='';
			if ($params->get('visualwidth')){
				$visualstyle.='width: '.$params->get('visualwidth').'px; ';
			}
			if ($params->get('visualtop',false)!==false){
				$visualstyle.='top: '.$params->get('visualtop').'px; ';
			};
			if ($params->get('visualleft',false)!==false){
				$visualstyle.='left: '.$params->get('visualleft').'px; ';
			}
			$html .= '<div class="teaservisual'.$params->get('visualclass','').'" style="'.$visualstyle.'">';
			if ($params->get('visualimage')){
				$html .= '<img class="" alt="'.$params->get('visualimagealt').'" title="'.$params->get('visualimagetitle').'" src="'.$params->get('visualimage').'" />';
			}
			$html .= '<div>'.$params->get('visualtext','&nbsp;').'</div>';
			$html .= '</div>';
		}
		return $html;
	}
	/**
	 * Holt die Daten aus dem Joomla-Beitrag (Article) mit der übergebenen ID
	 * @param integer $articleid Article ID des Beitrags
	 * @return array
	 */
    static function getArticle($articleid)
    {
		$data=array();
		$articleid=(int)$articleid;
		if ($articleid>0)
		{
			$article = JTable::getInstance('content');
			$article->load($articleid);
			$uri      = JUri::getInstance();
			$base     = $uri->toString(array('scheme', 'host', 'port'));
			$articleslug=$article->id.':'.$article->alias;
			$data['link'] = $base . JRoute::_(ContentHelperRoute::getArticleRoute($articleslug, $article->catid), false);
			$data['alias'] = $article->get('alias');
			$data['introtext'] = $article->get('introtext');
			$data['fulltext'] = $article->get('fulltext');
			$data['title'] = $article->get('title');
			$data['metadesc'] = $article->get('metadesc');
			$data['images'] = json_decode($article->get('images'),true);
		}
		return $data;
    }
	
	/**
	 * 
	 * @param type $image
	 * @return string
	 */
    static function getInlineStyle($image)
    {
		$ret ='style="background-image: ';
		$ret.="url('/".$image."')".'"';
		return $ret; 
    }
	
    /**
     * Retrieves the hello message
     *
     * @param array $params An object containing the module parameters
     * @access public
     */
    static function getModulContent( $params,$modul,$attribs )
    {
		$modulcontents=array();
		
	
		// Menüverknüpfung verarbeiten
		if ($params->get('menuitem')) {
			$application = JFactory::getApplication();
			$menu = $application->getMenu();
			$menuitem=$menu->getItem($params->get('menuitem'));
			$params->set('menuitemroute', $menu->getItem($params->get('menuitem'))->route);
			$params->set('menuurl'      , JRoute::_($menuitem->link . "&Itemid=" . $menuitem->id));
		}
	
		// Artikel Verknüpfung
		$articleId=$params->get('articleid',0);
		if ($articleId) {
			$article=self::getArticle($articleId);
			$params->set('articel',$article);
			$params->set('articellink',$article['link']);
			if ($params->get('articlecontent',0)) {
				// Artikel Text soll verwendet werden
				if ($params->get('articleheadlinesplit')) {
					$headlines=explode($params->get('articlesplitchars'),$article['title']);
					if (!$params->get('headline1')) {
					$params->set('headline1',$headlines[0]);
					}
					if (!$params->get('headline2')) {
						$params->set('headline2',$headlines[1]);
					}
				}
				else {
					if (!$params->get('headline1')) {
					$params->set('headline1',$headlines[0]);
					}
				}
				
				if (!$params->get('claim1')) {
					 $params->set('claim1',$article['metadesc']);
				}
			}
			if ($params->get('articleimage')) {
				// Artikel Bild soll verwendet werden
				if ($article['images']['image_intro']) {
					// Es gibt ein Beitrags Bild
					if (!$params->get('image'))	{
						$params->set('image',$article['images']['image_intro']);
					}
					if (!$params->get('imagetitle'))	{
						$params->set('imagetitle',$article['images']['image_intro_caption']);
					}
					if (!$params->get('imagealt'))	{
						$params->set('imagealt',$article['images']['image_intro_alt']);
					}
				}
			}
		}

		// Modul Verlinkung
		$modulurl='';
		switch ($params->get('linktype'))
		{
			case 0: $modulurl=$params->get('menuurl',''); break;
			case 1: $modulurl=$params->get('articellink',''); break;
			case 2: $modulurl=$params->get('externurl',''); break;
		}
		$params->set('modulurl',$modulurl);
		
		if ($params->get('backgroundimage')){
			$params->set('inlinestyle',self::getInlineStyle($params->get('image')));
		}

	//Noting to return. All Content now in the module params
    }
}

