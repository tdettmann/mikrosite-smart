<?php
/**
 * Joomla-Modul GU Teaser

 * 
 * @version	1.1.0
 * @package	Joomla.Site
 * @subpackage	gu_teaser
 * @author      Thomas Langer (TL)
 * @copyright	Copyright (C) 2013 GU GROUP Communications, Consulting & Technologies, Karlsruhe.
 * @todo        Code vollständig dokumentieren
 * 
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$hclass='';
if ($params->get('headlineclass')) {
	$hclass=' class="'.$params->get('headlineclass').'"';
}
if ($params->get('mobilcenter')) {
	$img_class=" t_img_center";
}

$html .= '    <div class="image">';
$html .= '		<img class="'.$img_class.'" alt="'.$params->get('imagealt').'" src="'.$params->get('image').'" />';
$html .= '    </div>';

$html .= '    <div class="image2">';
$html .= '		<img class="'.$img_class.'" alt="'.$params->get('image2alt').'" src="'.$params->get('image2').'" />';
$html .= '    </div>';

$html .= '    <div class="headlines '.$params->get('headlineclass','').'">';
$html .= '		<h3'.$hclass.'><span>'.$params->get('headline1').'</span></h3>';
if ($params->get('headline2')){
$html .= '		<h4'.$hclass.'><span>'.$params->get('headline2').'</span></h4>';
}
$html .= '    </div>';

$html .= '    <div class="description">';
if ($params->get('headline2')){
$html .= '	    <h4'.$hclass.'>'.$params->get('headline2').'</h4>';
}
if ($params->get('claim1')){
$html .= '	    <p class="claim1">'.$params->get('claim1').'</p>';
}
if ($params->get('claim1')){
$html .= '	    <p class="claim2">'.$params->get('claim2').'</p>';
}
if ($params->get('readmore')){
$html .= '	    <p class="readmore">'.$params->get('readmore').'</p>';
}
$html .= '    </div>';
