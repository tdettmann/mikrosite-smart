<?php
/**
 * Joomla-Modul GU Teaser

 * 
 * @version	1.1.0
 * @package	Joomla.Site
 * @subpackage	gu_teaser
 * @author      Thomas Langer (TL)
 * @copyright	Copyright (C) 2013 GU GROUP Communications, Consulting & Technologies, Karlsruhe.
 * @todo        Code vollständig dokumentieren
 * 
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$hclass='';
$style='';

if ($params->get('headlinetop',false)!==false){
	$style.='top: '.$params->get('headlinetop').'px; ';
};
if ($params->get('headlineleft',false)!==false){
	$style.='left: '.$params->get('headlineleft').'px; ';
}
	
if ($params->get('headlineclass')) {
	$hclass=' class="'.$params->get('headlineclass').'"';
}
if ($params->get('mobilcenter')) {
	$img_class=" t_img_center";
}


if (!$params->get('backgroundimage')){
	$html .= '    <div class="image">';
	$html .= '		<img class="'.$img_class.'" alt="'.$params->get('imagealt').'" src="'.$params->get('image').'" />';
	$html .= '    </div>';
}

$html .= '    <div class="headlines '.$params->get('headlineclass','').'" style="'.$style.'">';
$html .= '		<h3'.$hclass.'><span>'.$params->get('headline1').'</span></h3>';
if ($params->get('headline2')){
$html .= '		<h4'.$hclass.'><span>'.$params->get('headline2').'</span></h4>';
}
$html .= '    </div>';

if ($params->get('readmore')){
	$html .= '    <div class="media">';
	$html .= '	    <div class="readmore '.$params->get('calltoactionclass','').'"><span>'.$params->get('readmore').'</span></div>';
	$html .= '    </div>';
}
