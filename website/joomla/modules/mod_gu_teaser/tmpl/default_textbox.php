<?php
/**
 * Joomla-Modul GU Teaser

 * 
 * @version	1.1.0
 * @package	Joomla.Site
 * @subpackage	gu_teaser
 * @author      Thomas Langer (TL)
 * @copyright	Copyright (C) 2013 GU GROUP Communications, Consulting & Technologies, Karlsruhe.
 * @todo        Code vollständig dokumentieren
 * 
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// Überschriften ausgeben
// Class Attibut erzeugen
$hclass='';
if ($params->get('headlineclass')) {
	$hclass=' class="'.$params->get('headlineclass').'"';
}
// Ausgeben
if ($params->get('headline1')){
	$html .= '<h3'.$hclass.'><span>'.$params->get('headline1').'</span></h3>';
	if ($params->get('headline2')){
		$html .= '<h4'.$hclass.'><span>'.$params->get('headline2').'</span></h4>';
	}
}
else {
	if ($params->get('headline2')){
		$html .= '<h3'.$hclass.'><span>'.$params->get('headline2').'</span></h3>';
	}
}

// Weiterlesen Link
$plink = ''; // Link als Absatz nach den Texten
$tlink = ''; // Link ans Textende
// Link-Text gesetzt?
if ($params->get('readmore')){
	$readmore = '<span class="readmorelink">'.$params->get('readmore').'</span>';
	if ($params->get('calltoaction')){
		$readmore = '<a href="'.$params->get('modulurl').'">'.$readmore.'</a>';
	}
	// Link als Absatz
	if (!$params->get('readmoretype',0)){
		$plink = '<p class="readmore">'.$readmore.'</p>';
	}
	else {
		$tlink = ' '.$readmore;
	}
}

// Texte / Claims Ausgeben
if ($params->get('claim1')){
	$html .= '<p class="claim1">';
	$html .= $params->get('claim1');
	if (!$params->get('claim2')){
		$html .= $tlink;
	}
	$html .= '</p>';
}
if ($params->get('claim2')){
	
	$html .= '<p class="claim2">';
	$html .= $params->get('claim2').$tlink;
	$html .= '</p>';
}
$html .= $plink; // Ausgabe Link als Absatz nach den Texten
