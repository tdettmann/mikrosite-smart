<?php
/**
 * Joomla-Modul GU Teaser

 * 
 * @version	1.1.0
 * @package	Joomla.Site
 * @subpackage	gu_teaser
 * @author      Thomas Langer (TL)
 * @copyright	Copyright (C) 2013 GU GROUP Communications, Consulting & Technologies, Karlsruhe.
 * @todo        Code vollständig dokumentieren
 * 
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$hclass='';
$img_class='';
$style='';


// Headlin Classand Style
if ($params->get('headlineclass')) {
	$hclass=' class="'.$params->get('headlineclass').'"';
}
if ($params->get('headlinetop',false)!==false){
	$style.='top: '.$params->get('headlinetop').'px; ';
}
if ($params->get('headlineleft',false)!==false){
	$style.='left: '.$params->get('headlineleft').'px; ';
}


if ($params->get('mobilcenter')) {
	$img_class=" t_img_center";
}

// Image
$html .= '    <div class="image">';
$html .= '		<img class="'.$img_class.'" alt="'.$params->get('imagealt').'" src="'.$params->get('image').'" />';
$html .= '    </div>';


// Headlines
$html .= '<div class="headlines '.$params->get('headlineclass','').'" style="'.$style.'">';
if ($params->get('headline1')){
	$html .= '<h3'.$hclass.'><span>'.$params->get('headline1').'</span></h3>';
	if ($params->get('headline2')){
		$html .= '<h4'.$hclass.'><span>'.$params->get('headline2').'</span></h4>';
	}
}
else {
	if ($params->get('headline2')){
		$html .= '<h3'.$hclass.'><span>'.$params->get('headline2').'</span></h3>';
	}
}
$html .= '</div>';

if ($params->get('readmore')){
	$readmore='<span>'.$params->get('readmore').'</span>';
	$style='';
	if ($params->get('calltoaction',0)){
		$readmore='<a href="'.$params->get('modulurl').'">'.$readmore.'</a>';
		if ($params->get('calltoactionwidth')){
			$style.='width: '.$params->get('calltoactionwidth').'px; ';
		}
		if ($params->get('calltoactiontop',false)!==false){
			$style.='top: '.$params->get('calltoactiontop').'px; ';
		};
		if ($params->get('calltoactionleft',false)!==false){
			$style.='left: '.$params->get('calltoactionleft').'px; ';
		}
	}
	if($params->get('readmoreview',1)){
		$html .= '    <div class="calltoaction '.$params->get('calltoactionclass','').'" style="'.$style.'">';
		$html .= '	    <div class="readmore '.$params->get('calltoactionclass','').'">'.$readmore.'</div>';
		$html .= '    </div>';
	}
}