<?php
/**
 * Template für Joomla-Modul GU Teaser
 * 
 * @version	1.0.1
 * @package	Joomla.Site
 * @subpackage	gu_teaser
 * @author      Friedrich Schroedter (FS), Thomas Langer (TL)
 * @copyright	Copyright (C) 2013 GU GROUP Communications, Consulting & Technologies, Karlsruhe.
 * 
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$html = '<div class="guteaserbox'.$params->get('moduleclass_sfx').'">';

// Box-Layout laden
switch ($params->get('teasertype')) {
    case 0: // 0 = Image
	    $boxclass='teaser-image';
	    require JModuleHelper::getLayoutPath('mod_gu_teaser', 'default_imagebox');
	    break;
    case 1: // 1 = Text
	    $boxclass='teaser-text';
	    require JModuleHelper::getLayoutPath('mod_gu_teaser', 'default_textbox');
	    break;
	case 3: // 3 = Media
	    $boxclass='teaser-media';
	    require JModuleHelper::getLayoutPath('mod_gu_teaser', 'default_mediabox');
	    break;
	case 4: // 2 = Text mit 2 Bildern
	    $boxclass='teaser-type4';
	    require JModuleHelper::getLayoutPath('mod_gu_teaser', 'default_type4');
		break;
    default: // 2 = Text Bild
	    $boxclass='teaser-imagetext';
	    require JModuleHelper::getLayoutPath('mod_gu_teaser', 'default_imagetextbox');
}

// Deko-Element ausgeben
if ($params->get('teserboxmore')) {
	$html .= '<div class="teaserboxmore">'.$params->get('teserboxmoretext','&nbsp;').'</div>';
}

// Visual-Element ausgeben
$html .= modGuTeaserHelper::getvisual($params);


$html .= '</div>';

// Call to action? 
// Ja  : Nur das Call to action Element ist verlinkt
// Nein: Standard, Der gesamte Inhalt der Box ist in ein Link gepackt
if (!$params->get('calltoaction',0)){
	$html = '<a class="teaserboxlink" href="'.$params->get('modulurl').'">'.$html.'</a>';
}

// Modulcontent ausgeben
echo $html;