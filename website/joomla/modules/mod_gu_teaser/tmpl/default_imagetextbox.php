<?php
/**
 * Joomla-Modul GU Teaser

 * 
 * @version	1.1.0
 * @package	Joomla.Site
 * @subpackage	gu_teaser
 * @author      Thomas Langer (TL)
 * @copyright	Copyright (C) 2013 GU GROUP Communications, Consulting & Technologies, Karlsruhe.
 * @todo        Code vollständig dokumentieren
 * 
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$hclass='';
$style='';
$img_class='';

if ($params->get('headlineclass')) 
{
	$hclass=' class="'.$params->get('headlineclass').'"';
}

if ($params->get('mobilcenter')) 
{
	$img_class=" t_img_center";
}

if ($params->get('headlinetop',false)!==false)
{
	$style.='top: '.$params->get('headlinetop').'px; ';
}

if ($params->get('headlineleft',false)!==false)
{
	$style.='left: '.$params->get('headlineleft').'px; ';
}

//$html .= '<a href="'.$params->get('modulurl').'">';

// 1. Kachel
// Bild ausgeben
$html .= '<div class="image">';
$html .= ' <img class="'.$img_class.'" alt="'.$params->get('imagealt').'" src="'.$params->get('image').'" />';
$html .= '</div>';

$html .= '<div class="headlines '.$params->get('headlineclass','').'" style="'.$style.'">';

if (!$params->get('headline1'))
{
	if ($params->get('headline2'))
	{
		$html .= '<h3'.$hclass.'><span>'.$params->get('headline2').'</span></h3>';
	}
}
else 
{
	$html .= '<h3'.$hclass.'><span>'.$params->get('headline1').'</span></h3>';
	if ($params->get('headline2'))
	{
		$html .= '<h4'.$hclass.'><span>'.$params->get('headline2').'</span></h4>';
	}
}
$html .= '</div>';

// 2. Kachel
$html .= '<div class="description '.$params->get('descriptionclass','').'">';
if ($params->get('headline2'))
{
	$html .= '<h4'.$hclass.'>'.$params->get('headline2').'</h4>';
}
else 
{
	if ($params->get('headline1'))
	{
		$html .= '<h4'.$hclass.'>'.$params->get('headline1').'</h4>';
	}
}
// Weiterlesen Link
$plink = ''; // Link als Absatz nach den Texten
$tlink = ''; // Link ans Textende
// Link-Text gesetzt?
if ($params->get('readmore'))
{
	$readmore = '<span class="readmorelink '.$params->get('calltoactionclass','').'">'.$params->get('readmore').'</span>';
	if ($params->get('calltoaction'))
	{
		$readmore = '<a href="'.$params->get('modulurl').'">'.$readmore.'</a>';
	}
	
	// Link als Absatz
	if (!$params->get('readmoretype',0))
	{
		$plink = '<p class="readmore '.$params->get('calltoactionclass','').'">'.$readmore.'</p>';
	}
	else 
	{
		$tlink = ' '.$readmore;
	}
}

// Texte / Claims Ausgeben
if ($params->get('claim1'))
{
	$html .= '<p class="claim1">';
	$html .= $params->get('claim1');
	if (!$params->get('claim2'))
	{
		$html .= $tlink;
	}
	$html .= '</p>';
}

if ($params->get('claim2'))
{
	
	$html .= '<p class="claim2">';
	$html .= $params->get('claim2').$tlink;
	$html .= '</p>';
}
$html .= $plink; // Ausgabe Link als Absatz nach den Texten
$html .= '</div>';
