<?php
/**
 * Joomla-Modul GU Teaser

 * 
 * @version	1.0.1
 * @package	Joomla.Site
 * @subpackage	gu_teaser
 * @author      Friedrich Schroedter (FS), Thomas Langer (TL)
 * @copyright	Copyright (C) 2013 GU GROUP Communications, Consulting & Technologies, Karlsruhe.
 * @todo        Code vollständig dokumentieren
 * 
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
// include helper
require_once( dirname(__FILE__).'/helper.php' );

// prepare module content
$moduleclass_sfx=$params->get('moduleclass_sfx');


if (!$params->get('mobil')){
    $moduleclass_sfx.=$moduleclass_sfx.' no-mobile';
}

if (!$params->get('desktop')){
    $moduleclass_sfx.=$moduleclass_sfx.' no-desktop';
}

if ($params->get('teasercols',1)>=2){
    $moduleclass_sfx.=$moduleclass_sfx.' box'.$params->get('teasercols').'cols';
}

$mobil=false;
if (defined( 'ISMOBIL' )) { $mobil=ISMOBIL; }

// Mobil-Gerät und Box soll Mobil nicht angezeigt werden -> Keine Ausgabe
if ($mobil && !$params->get('mobil',true)) {
	return false;
}

// Kein Mobil-Gerät -> Desktop und Box soll auf den Desktop nicht angezeigt werden -> Keine Ausgabe
if (!$mobil && !$params->get('desktop',true)) {
	return false;
}

$calltoactionclass='';
if ($params->get('calltoaction',0))
{
	$calltoactionclass=' calltoactionbox';
}
// Ausgabe
switch ($params->get('teasertype')) {
    case 0: $params->set('moduleclass_sfx',$moduleclass_sfx.' teaser-image'.$calltoactionclass); break; // 0 = Image
    case 1: $params->set('moduleclass_sfx',$moduleclass_sfx.' teaser-text'.$calltoactionclass); break; // 1 = Text
	case 3: $params->set('moduleclass_sfx',$moduleclass_sfx.' teaser-media'.$calltoactionclass); break; // 3 = Text
	case 4: $params->set('moduleclass_sfx',$moduleclass_sfx.' teaser-type4'.$calltoactionclass); break; // 4 = Text
    default: $params->set('moduleclass_sfx',$moduleclass_sfx.' teaser-imagetext'.$calltoactionclass); // 2 Image+Text
}

$params->set('module_id',$module->id);


$modulcontents = modGuTeaserHelper::getModulContent( $params,$module,$attribs);

// output modul content
//if (($attribs['ismobile']==1) & !$params->get('mobile'))
require JModuleHelper::getLayoutPath('mod_gu_teaser');