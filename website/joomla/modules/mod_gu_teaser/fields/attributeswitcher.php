<?php
/**
 * Joomla-Feldtyp Attributeswitcher mod_gu_fields 
 * 
 * @version	0.0.1
 * @package	Joomla.Site
 * @subpackage	mod_gu_fields
 * @author      Thomas Langer
 *
 */

jimport('joomla.html.html');

JFormHelper::loadFieldClass('list');

/**
 * Formular Feld Klasse für die Joomla Platform.
 * Unterstützt eine algemeine Liste von Optionen, abgeleitet von JFormFieldList.
 *
 * @package     Joomla.Platform
 * @subpackage  Form
 * @since       11.1
 * @author      Thomas Langer
 * see          Class JFormFieldList ... libraries/joomla/form/fields/list.php
 */
class JFormFieldAttributeSwitcher extends JFormFieldList {
    /**
     * @var    string  Der Formular Feld Typ.
     * @since  11.1
     */
    protected $type = 'attributeswitcher';
 
    /**
     * Methode Erzeugt ein zum Feld gehörendes Javascript das
     * die gruppierten Felder im Joomla Backend ein- und ausblendet.
     * 
     * @return  string  Das zum Feld gehörende Javascript.
     */
    private function getScript()
    {
//        $myclass = $this->element['class'];
		$classes = explode(' ', $this->element['class'],2);
        $myclass = $classes[0];
        $myid= $this->id;
        $js= <<<jsscript
            <script type="text/javascript">
                jQuery(document).ready(
                    function ()
                    { 
                        var attrswcgs = jQuery("body").find( ".$myclass" );
                        jQuery(attrswcgs).each(
                            function()
                            { 
                                if ( jQuery(this).attr("id")!="{$myid}" )
                                { 
                                    if (!jQuery(this).hasClass("nogroup"))
                                    {
										if (typeof jQuery.data(this,'viewc') === "undefined")
										{ jQuery.data(this,'viewc',0); }
								
                                        jQuery(this).on(
                                            "{$myclass}UpdateView",
                                            function(event,groupnr)
                                            {
                                                if ( groupnr=='showallfields' )
											    {
													jQuery(this).closest(".control-group").show();
												}
                                                else
                                                {
                                                    if ( jQuery(this).hasClass("{$myclass}_"+groupnr) )
													{
                                                        jQuery(this).closest(".control-group").show();
													}
                                                    else
                                                    {
                                                        jQuery(this).closest(".control-group").hide();
													}
                                                }
                                            }
                                        );
                                     };
                                };
                            }
                        ); 
                        jQuery(".{$myclass}").trigger( "{$myclass}UpdateView", [ jQuery("#{$myid}").val() ] );
                        jQuery("#{$myid}").change(
                            function()
                            {	
                                jQuery(".{$myclass}").trigger( "{$myclass}UpdateView", [ jQuery("#{$myid}").val() ] );
								
								var myclasses = jQuery(this).attr('class');
								
								//alert(myclasses);
								var triggerclasses = myclasses.match(/trigger_\w+/g);
								if (triggerclasses !== null)
								{
									for (var i = 0; i < triggerclasses.length; ++i)
									{	
										var tclass= triggerclasses[i].split(/_/)
										var tc= '.'+tclass[1];
										jQuery(tc).trigger('change');
									}
								}
                            }
                        );
                    }
                );
            </script>
jsscript;
        return $js;
    }
    
    /**
     * Erzeugt den HTML-Markup für das Eingabefeld
     * Erweitert die Methode aus JFormFieldList 
     * 
     * @see     JFormFieldList -> getInput
     * @return  string  HTML-Markup für das Eingabefeld mit dem zugehörigen Javascript.
     */

    function getInput() {
        $html=$this->getScript().parent::getInput();
        return $html;
    }
}