Dokumentation Joomla-Feldtyp Attributeswitcher

Verwendung

In der der XML-Datei des Moduls wurde das Element <fields> oder <field> um den Parameter 
addfieldpath ergänzt. Er enthält die Pfad-Angabe zur Datei attributeswitcher.php
Diese Angabe ist notwendig damit Joomla den neuen Feldtyp findet.

<fields name="params" addfieldpath="/modules/mod_MODULNAME/fields">

Das Feld Attributeswitcher wird so angelegt.
<field 
    name="gruppe" 
    type="attributswitcher"
    default="0"
    label="Gruppe" 
    description="Gruppe auswählen"
    class="attrswitch">
    <option value="0">Keine Gruppe</option>
    <option value="1">Gruppe 1</option>
    <option value="2">Gruppe 2</option>
    <option value="3">Gruppe 3</option>
 </field>

Um Felder zu gruppieren, muss im Element <field> der Parameter class angegeben 
werden. Alle Felder mit gleichem Wert für class gehören dann zu einer Gruppe. 
Die Felder die mit dem Attributeswitcher gruppiert werden sollen enthalten 
mindestens zwei Werte in class. Z.B. class="attrswitch group_1". Der erste Wert 
ist der selbe wie beim Attributeswitcher und verbindet das Feld mit ihm. Die 
weiteren Werte gruppieren die Felder miteinander. Hierbei werden die Werte der 
option-Elemente vom Attributeswitcher durch ein vorgestelltes group_ ergänzt.
Soll ein Feld zu mehr als einer Gruppe gehören, so gibt man einfach einen 
weiteren Wert in class am. Z.B. class="attrswitch group_1 group_1"
Soll ein Feld zu keiner Gruppe gehören, so muss der Wert nogroup hinzugefügt 
werden. Z.B. class="attrswitch nogroup"

Hat ein option-Element den Wert 0 (Null) so werden alle Felder, auser den nogroup 
Felder, ausgeblendet.

Hat ein option-Element den Wert showallfields so werden alle Felder angezeigt.

Hier ein Beispiel

<field 
    name="gruppe" 
    type="attributswitcher"
    default="0"
    label="Gruppe" 
    description="Gruppe auswählen"
    class="myattrswitch">
    <option value="0">Keine Gruppe</option>
    <option value="1">Gruppe 1</option>
    <option value="2">Gruppe 2</option>
</field>
<field 
  name="textfeld1"
  type="text"
  default="textfeld1"
  label="Textfeld1"
  description="Textfeld1"
  class="myattrswitch group_1" />
<field 
  name="textfeld2"
  type="text"
  default="textfeld2"
  label="Textfeld2"
  description="Textfeld2"
  class="myattrswitch group_1 group_2" />
<field 
  name="textfeld3"
  type="text"
  default="textfeld3"
  label="Textfeld3"
  description="Textfeld3"
  class="myattrswitch nogroup" />
