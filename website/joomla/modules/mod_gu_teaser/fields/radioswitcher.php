<?php
/**
 * Joomla-Feldtyp Attributeswitcher mod_gu_fields 
 * 
 * @version	0.0.1
 * @package	Joomla.Site
 * @subpackage	mod_gu_fields
 * @author      Thomas Langer
 *
 */

jimport('joomla.html.html');

JFormHelper::loadFieldClass('radio');

/**
 * Formular Feld Klasse für die Joomla Platform.
 * Unterstützt algemeine Radio-Buttons, abgeleitet von JFormFieldRadio.
 *
 * @package     Joomla.Platform
 * @subpackage  Form
 * @since       11.1
 * @author      Thomas Langer
 * see          Class JFormFieldRadio ... libraries/joomla/form/fields/radio.php
 */
class JFormFieldRadioSwitcher extends JFormFieldRadio {
    /**
     * @var    string  Der Formular Feld Typ.
     * @since  11.1
     */
    protected $type = 'radioswitcher';
 
    /**
     * Methode Erzeugt ein zum Feld gehörendes Javascript das
     * die gruppierten Felder im Joomla Backend ein- und ausblendet.
     * 
     * @return  string  Das zum Feld gehörende Javascript.
     */
    private function getScript()
    {
		$classes = explode(' ', $this->element['class'],2);
        $myclass = $classes[0];
        $myid    = $this->id;
		$myname  = $this->name;
        $js= <<<jsscript
            <script type="text/javascript">
                jQuery(document).ready(
                    function ()
                    { 
//                        var radioswcgs = jQuery("body").find( ".$myclass" );
//                        jQuery(radioswcgs).each(
//                            function()
//                            { 
//                                if ( jQuery(this).attr("id")!="{$myid}" )
//                                { 
//                                    if (!jQuery(this).hasClass("nogroup"))
//                                    {
//                                        jQuery(this).on(
//                                            "{$myclass}UpdateView",
//                                            function(event,groupnr)
//                                            {
//                                                if ( groupnr=='showallfields' )
//                                                    jQuery(this).closest(".control-group").show();
//                                                else
//                                                {
//                                                    if ( jQuery(this).hasClass("{$myclass}_"+groupnr) )
//                                                        jQuery(this).closest(".control-group").show();
//                                                    else
//                                                        jQuery(this).closest(".control-group").hide();
//                                                }
//                                            }
//                                        );
//                                     };
//                                };
//                            }
//                        ); 
//                        jQuery(".{$myclass}").trigger( "{$myclass}UpdateView", [ jQuery("input[name='{$myclass}']:checked").val() ] );
//						jQuery("input[name='{$myname}']").change(
//                            function()
//                            {
//								alert(jQuery("input[name='{$myclass}']:checked").val());
//                                //jQuery(".{$myclass}").trigger( "{$myclass}UpdateView", [ jQuery("input[name='{$myname}']:checked").val() ] );  
//                            }
//                        );
                    }
                );
            </script>
jsscript;
        return $js;
    }
    
    /**
     * Erzeugt den HTML-Markup für das Eingabefeld
     * Erweitert die Methode aus JFormFieldList 
     * 
     * @see     JFormFieldRadio -> getInput
     * @return  string  HTML-Markup für das Eingabefeld mit dem zugehörigen Javascript.
     */

    function getInput() {
        $html=$this->getScript().parent::getInput();
		//$html=parent::getInput();
        return $html;
    }
}