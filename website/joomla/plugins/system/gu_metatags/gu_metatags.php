<?php
/**
 * Joomla-Plugin GU Metatags.
 * Fügt zusätzliche Metatags in den Html-Head ein. 
 * Einstellungen für SEO, GEO-Position, Touchicons Viewport usw.
 * 
 * @version	3.5
 * @package	Joomla.Site
 * @subpackage	Templates.swp
 * @author      GU KOMMUNIKATION Karlsruhe, Thomas Langer (TL)
 * @copyright	Copyright (C) 2014 GU GROUP Communications, Consulting & Technologies, Karlsruhe.
 */

// No direct access.
defined('_JEXEC') or die;

class PlgSystemGu_Metatags extends JPlugin
{
	var $document = null; // enthält Verweis auf JFactory::getDocument();
	var $openGraphProtocolEnabled = false; // True wenn die Open Graph Protocol Daten ausgegeben wurden
	
	/*
	public function __construct(&$subject, $config)
	{
		parent::__construct($subject, $config);
		$this->loadLanguage();
	}
	*/
	
	/**
	 * Setzt einen Metatag
	 * @param type $name String Neme des MetaTags
	 * @param type $value String Wert
	 */
    private function setMetaTag($name, $value) 
	{
		if ($value) {
			$this->document->setMetaData( $name, htmlspecialchars($value) );
		}
    }
	
	/**
	 * Bereinigt eine Positionsangabe und speichert sie zurück in den Plugin-Parameter
	 */
	private function prepareGeoPosition($key,$default='')
	{
		$temp = htmlspecialchars($this->params->get($key,$default));
		$temp = str_replace(array(",", " "), array(".", ""), $temp );
		$this->params->set($key,$temp);
	}
	
	/**
	 * Erzeugt Geo Metatags
	 */
	private function metaGeoPosition()
	{
		if ($this->params->get('metatag_geo_region')){
			$this->setMetaTag('geo.region', $this->params->get('metatag_geo_region'));
		}
		elseif ($this->params->get('metatag_geo_country')){
			$this->setMetaTag( 'geo.country', $this->params->get('metatag_geo_country'));
		}
		if ($this->params->get('metatag_geo_placename')){
			$this->setMetaTag( 'geo.placename', $this->params->get('metatag_geo_placename') );
		}
		if ($this->params->get('metatag_geo_latitude')) {
			// direktes setzen, da Koordinaten bereits bereinigt wurden
			$this->document->setMetaData( 'geo.position', $this->params->get('metatag_geo_latitude') . ';' . $this->params->get('metatag_geo_longitude') );	
		}
	}
	
	/**
	 * Erzeugt ICBM Geo-Metatag (intercontinental ballistic missile)
	 */
	private function metaIcbmPosition()
	{
		if ($this->params->get('metatag_geo_latitude')) {
			// direktes setzen, da Koordinaten bereits bereinigt wurden
			$this->document->setMetaData( 'ICBM',  $this->params->get('metatag_geo_latitude') . ',' . $this->params->get('metatag_geo_longitude'));
			
		}
	}
	
	/**
	 * Erzeugt Apple IOS spezifische Metatags
	 */
	private function metaAppleIos()
	{
		$metatag_apple_app_capable = htmlspecialchars($this->params->get('metatag_apple_app_capable',''));
		$metatag_apple_statusbar = htmlspecialchars($this->params->get('metatag_apple_statusbar',''));
		$metatag_apple_startup_image = htmlspecialchars($this->params->get('metatag_apple_startup_image',''));
		// Apple APP
		if ($metatag_apple_app_capable){
			$document->setMetaData( 'apple-mobile-web-app-capable', 'yes' );
		}
		if ($metatag_apple_statusbar){
			$document->setMetaData( 'apple-mobile-web-app-status-bar-style', $metatag_apple_statusbar );
		}
//		if ($metatag_apple_startup_image){
//			$document->setMetaData( 'apple-touch-startup-image', JURI::current().$metatag_apple_startup_image );
//   	}
	}
	
	/**
	 * Apple touch icons
	 */
	private function metaTouchIcons()
	{
		$folder = $this->params->get('metatag_folder_touchicons','');
		if ($folder) {
			$folder = '/images/'.$folder;
			$iconrel='apple-touch-icon';
			if (!$this->params->get('metatag_apple_icons_precomposed',0)){
				$iconrel='apple-touch-icon-precomposed';
			}
			$this->document->addCustomTag('<link rel="'.$iconrel.'" sizes="144x144" href="' . $folder . '/apple-touch-icon-ipad-retina.png" />');
			$this->document->addCustomTag('<link rel="'.$iconrel.'" sizes="112x114" href="' . $folder . '/apple-touch-icon-iphone-retina.png" />');
			$this->document->addCustomTag('<link rel="'.$iconrel.'" sizes="72x72" href="'   . $folder . '/apple-touch-icon-ipad.png" />');
			$this->document->addCustomTag('<link rel="'.$iconrel.'" sizes="57x57" href="'   . $folder . '/apple-touch-icon-iphone.png" />');
		}
	}
	
	/**
	 * GooglePlus
	 */
	private function metaGooglePlus() {
		if ($this->params->get('metatag_googleplus_enable'))
		{
			if ($metatag_googleplus_autor)
			{
				$metatag_googleplus_autor    = htmlspecialchars($this->params->get('metatag_googleplus_autor',''));
				$metatag_googleplus_profilurl= htmlspecialchars($this->params->get('metatag_googleplus_profilurl',''));
				$gp_attribs = array('title' => $metatag_googleplus_autor );
				$document->addHeadLink( $metatag_googleplus_profilurl,'author', 'rel', $gp_attribs );	
				// erzeugt: <link rel="author" href="https://plus.google.com/PROFIL" title="NAMEDESPFOFIL" />
			}
		}
	}
	
	/**
	 * Facebook
	 */
	private function metaFacebook()
	{
		if ($this->params->get('metatag_fb_enable')){
			if ($this->params->get('metatag_fb_pageid')){
				$this->setMetaTag( 'fb:page_id', $this->params->get('metatag_fb_pageid') );
			}
			if ($this->params->get('metatag_fb_admins')){
				$document->setMetaData( 'fb:admins', $this->params->get('metatag_fb_admins') );
			}
//			$url = JURI::current();
//			$this->setMetaTag( 'fb:url', $url );	
//			$x = $this->document->getMetaData( 'title' );	
//			$this->setMetaTag( 'fb:url2', $this->document->title );	
		}
	}
	
	/**
	 * Twitter
	 */
	private function metaTwitter()
	{
		if ($this->params->get('metatag_twitter_enable')){
			$this->setMetaTag( 'twitter:card', "summary" );
			if ($metatag_og_image){
				$this->setMetaTag( 'twitter:image', JURI::current().$metatag_og_image );
			}
			// TODO: Beschreibung von Joomla ermitteln
			$this->setMetaTag( 'twitter:description', $this->document->getMetaData("description"));
			$this->setMetaTag( 'twitter:title', $this->document->title);

			if ($this->params->get('metatag_twitter_site')){
				$document->setMetaTag( 'twitter:site', $this->params->get('metatag_twitter_site') );
			}
			if ($this->params->get('metatag_twitter_creator')){
				$this->setMetaTag( 'twitter:creator', $this->params->get('metatag_twitter_creator'));
			}
		}
	}
	
	/**
	 * Kontaktdaten als Open Graph Protocol
	 */
	private function metaOpenGraphProtocolContact()
	{
			if($this->params->get('metatag_geo_contact_enable'))
			{
				if ($this->params->get('metatag_geo_street')){
					$this->setMetaTag( 'og:street-address', $this->params->get('metatag_geo_street') );
				}
				if ($this->params->get('metatag_geo_postal_code')){
					$this->setMetaTag( 'og:postal-code', $this->params->get('metatag_geo_postal_code') );
				}
				if ($this->params->get('metatag_geo_email')){
					$this->setMetaTag( 'og:email', $this->params->get('metatag_geo_email') );
				}
				if ($this->params->get('metatag_geo_phone')){
					$this->setMetaTag( 'og:phone_number', $this->params->get('metatag_geo_phone') );
				}
				if ($this->params->get('metatag_geo_fax')){
					$this->setMetaTag( 'og:fax_number', $this->params->get('metatag_geo_fax') );
				}
			}
	}
	/**
	 * Open Graph Protocol
	 */
	private function metaOpenGraphProtocol()
	{	
		$config = JFactory::getConfig();
		if ($this->params->get('metatag_og_enable')){
			if ($this->params->get('metatag_geo_region')){
				$this->setMetaTag( 'og:region', $this->params->get('metatag_geo_region') );
			}
			if ($this->params->get('metatag_geo_country')){
				$this->setMetaTag( 'og:country-name', $this->params->get('metatag_geo_country') );
			}
			if ($this->params->get('metatag_geo_placename')){
				$this->setMetaTag( 'og:locality', $this->params->get('metatag_geo_placename') );
			}
			if ($this->params->get('metatag_geo_position',0)){
				$geo_latitude = $this->params->get('metatag_geo_latitude','');
				$geo_longitude= $this->params->get('metatag_geo_longitude','');
				if ($geo_latitude && $geo_longitude) {
					$this->setMetaTag( 'og:latitude', $geo_latitude );
					$this->setMetaTag( 'og:longitude', $geo_longitude );
				}
			}
			$this->setMetaTag( 'og:title', $this->document->title );
			$this->setMetaTag( 'og:url', JURI::current() );
			// TODO: Webseitename von Joomla ermitteln
			$this->setMetaTag( 'og:site_name', $config->get( 'sitename' ));
			// TODO: Beschreibung von Joomla ermitteln
			$this->setMetaTag( 'og:description', $this->document->getMetaData("description") );
			if ($this->params->get('metatag_og_type')){
				$this->setMetaTag( 'og:type', $this->params->get('metatag_og_type') );
			}
			if ($this->params->get('metatag_og_image')){
				$this->setMetaTag( 'og:image', JURI::current().$this->params->get('metatag_og_image') );
				//<meta property="og:image:secure_url" content="https://secure.example.com/ogp.jpg" />
			}
			// Kontaktdaten Ausgeben
			$this->metaOpenGraphProtocolContact();
			$this->openGraphProtocolEnabled = true;
		}
	}

	/**
	 * Windows 8.1 Start Screen Kachel
	 * http://www.buildmypinnedsite.com/de-DE
	 */
	private function metaWindowsStartScreen() {
		
	}
	/**
	 * Windows7 IE 9 Pinning
	 */
	private function metaWindowsIE() {
		
	}
	/**
	 *  Plugin Verarbeitung
	 */
	public function onAfterDispatch()
	{
		// Check that we are in the site application.
		if (JFactory::getApplication()->isAdmin()){
			return true;
		}
		// Check if the metatags should be activated in this environment.
		if (JFactory::getDocument()->getType() !== 'html' ){
			return true;
		}
		
		// Initialisierung
		$this->document = JFactory::getDocument();
		$this->config = JFactory::getConfig();
		
		// Joomla Hinweis in HTML-Code verhindern (Meta Generator), wir auch von anderen Erweiterungen entfent.
		if ($this->params->get('metatag_generator_hide'))
			$this->document->setGenerator(null);
		
		// Ausgabe der Meta Tags
		// Geo Meta Tags
		$this->prepareGeoPosition('metatag_geo_latitude');
		$this->prepareGeoPosition('metatag_geo_longitude');
		if ($this->params->get('metatag_geo_position',0)){
			$this->metaGeoPosition();
		}
		if ($this->params->get('metatag_geo_icbm',0)){
			$this->metaIcbmPosition();
		}
		// Autor der Webseite. Supported in Joomla 3
		if ($this->params->get('metatag_author')){
			$this->setMetaTag( 'author', $this->params->get('metatag_author') );
		}
		// Copyright. Joomla 3 uses the metatag "rights"
		if ($this->params->get('metatag_copyright_joomla')=='1'){
			$this->document->setMetaData( 'copyright', $this->document->getMetaData("rights") ); 
		}
		else { 
			if ($this->params->get('metatag_copyright')){
				$this->setMetaTag( 'copyright', $this->params->get('metatag_copyright') );
			}
		}
		
		// Sprache der Website
		//$metatag_language_include = htmlspecialchars($this->params->get('metatag_language_include',0));
		//$language = substr( htmlspecialchars($this->params->get('metatag_language','')) ,0,2);
		//if ($metatag_language_include && $language){
		//	$This-Ydocument->addCustomTag('<meta http-equiv="content-language" content="' . $language . '" />');
		//}

		// Gestalter der Website. Sollte aber auch im Inhalt der Seite erscheinen.
		//if ($this->params->get('metatag_designer')){
		//	$this->setMetaTag( 'designer', $this->params->get('metatag_designer',''));
		//}
		
		// Google site verification key
		if ($this->params->get('metatag_googlekey')){
			$this->setMetaTag( 'google-site-verification', $this->params->get('metatag_googlekey') );
		}
			
		// Betriebsysteme und Browser
		//$this->metaWindowsStartScreen();
		//$this->metaWindowsIE();
		//Mobilgeräte
		//$this->metaAppleIos();
		$this->metaTouchIcons();
		
		// Mobil Darstellung Zoombar Viewport
		if ($this->params->get('metatag_viewport')){
			$this->setMetaTag( 'viewport', $this->params->get('metatag_viewport') );
		}
		
		// Open Graph Protocol
		$this->metaOpenGraphProtocol();
		
		// Soziale Netzwerke
		if ($this->openGraphProtocolEnabled){
			// Open Graph Protocol wurde ausgegeben
			// Vorraussetzung für Facebook und Twitter
			$this->metaFacebook();
			$this->metaTwitter();
		}
		$this->metaGooglePlus();

	}
}
