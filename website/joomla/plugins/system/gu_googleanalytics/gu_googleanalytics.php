<?php
/**
 * Joomla-Plugin GU Google Analytics
 * Gibt das Google Analytics Tracking Javascript aus.
 * 
 * @version	3.4
 * @package	Joomla.Site
 * @subpackage	gu_googleanalytics
 * @author      Friedrich Schroedter (FS), Thomas Langer (TL)
 * @copyright	Copyright (C) 2013 GU GROUP Communications, Consulting & Technologies, Karlsruhe.
 * @license	GNU General Public License version 2 or later; see LICENSE.txt
 * 
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin');
jimport( 'joomla.html.parameter');

class plgSystemGu_Googleanalytics extends JPlugin
{
	/**
	 * 
	 * @param type $subject
	 * @param type $config
	 */
	function plgSystemGu_Googleanalytics(&$subject, $config)
	{
		parent::__construct($subject, $config);
		$this->_plugin = JPluginHelper::getPlugin( 'system', 'gu_googleanalytics' );
		$this->_params = new JRegistry( $this->_plugin->params );
	}
	
	/**
	 * 
	 * @return boolean
	 */
	function onAfterRender()
	{
		$mainframe  = JFactory::getApplication();
		$googleid   = $this->params->get('google_id', '');
		$trackingjs = $this->params->get('trackingjs','');
		
		if(($googleid == '' & $trackingjs == '') || $mainframe->isAdmin() || strpos($_SERVER["PHP_SELF"], "index.php") === false)
		{
		    return;
		}
		
		$anonymizeip = $this->params->get('anonymizeip', '');
		$domainname  = $this->params->get('domainname', '');
		$codeposition= $this->params->get('codeposition', '');
		
		$buffer = JResponse::getBody();
		
		$googleanonymizeip='';
		$googledomainname='';
		if ($anonymizeip){
		    $googleanonymizeip = "  _gaq.push(['_gat._anonymizeIp']);"."\n";
		}
		if ($domainname){
		    $googledomainname = "  _gaq.push(['_setDomainName', '". $domainname ."']);"."\n";
		}
		
$js = <<<GOOGLEANALYTICS
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', '$googleid']);
  _gaq.push(['_trackPageview']);
$googleanonymizeip
$googledomainname		
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
GOOGLEANALYTICS;

//		$google_analytics_javascript = array();
//		$google_analytics_javascript[] = '<script type="text/javascript">';
//		$google_analytics_javascript[] = '  var _gaq = _gaq || [];';
//		$google_analytics_javascript[] = '  _gaq.push([\'_setAccount\', \'' . $googleid . '\']);';
//	
//  		if ($anonymizeip)
//		    $google_analytics_javascript[] = '  _gaq.push([\'_gat._anonymizeIp\']);';
//		if ($domainname)
//		    $google_analytics_javascript[] = '  _gaq.push([\'_setDomainName\', \'' . $domainname . '\']);';		
//		
//		$google_analytics_javascript[] = '  _gaq.push([\'_trackPageview\']);';
//		$google_analytics_javascript[] = '  (function() {';
//		$google_analytics_javascript[] = '   var ga = document.createElement(\'script\'); ga.type = \'text/javascript\'; ga.async = true;';
//		$google_analytics_javascript[] = '   ga.src = (\'https:\' == document.location.protocol ? \'https://ssl\' : \'http://www\') + \'.google-analytics.com/ga.js\';';
//		$google_analytics_javascript[] = '   var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(ga, s);';
//		$google_analytics_javascript[] = '  })();';
//		$google_analytics_javascript[] = '</script>';
//		
//		$trackingjs=implode("\n", $google_analytics_javascript);
		
		// Benutzer Trackincode verwenden
		if ($trackingjs){
			$js=$trackingjs;
		}
		if (!$codeposition)
		{
		    $buffer = str_replace ("</head>", $js . "</head>", $buffer);
		}
		else
		{
		    $buffer = str_replace ("</body>", $js . "</body>", $buffer);
		}
		JResponse::setBody($buffer);
		return true;
	}
}