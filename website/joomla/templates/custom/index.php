<?php
/**
 * Joomla-Template GletscherEnergie für die Stadtwerke Pforzheim.
 * 
 * @version	1.0
 * @package	Joomla.Site
 * @subpackage	Templates.experten-energie
 * @author      GU KOMMUNIKATION Karlsruhe
 * @copyright	Copyright (C) 2016 Gordana Uzelac, Agentur für Kommunikation - GU KOMMUNIKATION, Karlsruhe.
 * 
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// Initialisierung
include_once JPATH_THEMES.'/'.$this->template.'/includes/template_logic.php';

?><!DOCTYPE html>
<html xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>">
<head>
	<?php 
	// Inhalt des Head laden
	include_once JPATH_THEMES.'/'.$this->template.'/includes/template_head.php'; 
	?>
</head>
<body id="pabatemplate" class="<?php echo $pageclass_sfx; ?><?php echo (constant('ISMOBIL') ? ' mobil' : ''); ?>">
<?php 
if ($isOldBrowser){ 
	include_once JPATH_THEMES.'/'.$this->template.'/includes/template_oldbrowser.php'; 	// Alter Browser
}
?>
    <div id="page-wrapper">
		<div id="page">
			<?php 
			// Kopf-Bereich laden
			include_once JPATH_THEMES.'/'.$this->template.'/includes/template_page_header.php';
			
			// Content-Bereich laden
			include_once JPATH_THEMES.'/'.$this->template.'/includes/template_page_content.php';
			?>
		</div>
		<?php
			// Fuss-Bereich laden
			include_once JPATH_THEMES.'/'.$this->template.'/includes/template_page_footer.php';
		?>
    </div>
	<jdoc:include type="message" />
	<div id="bookmarkhint" class="opacity-wrapper page-center">
		<div class="opacity"></div>
		<div class="page-center">
			<?php echo ($isMobile ? $this->params->get('bookmarkhintmobil') : $this->params->get('bookmarkhint')); ?>
			<a class="close" onclick="jQuery('#bookmarkhint').toggle();" href="#bookmark"><?php echo $this->params->get('bookmarkclosetitle'); ?></a>
			<div class="clear"></div>
		</div>
    </div>
       
	<?php
	// Template Footer JavaScripts laden
	include_once JPATH_THEMES.'/'.$this->template.'/includes/template_footer_js.php';
	?>
        
        
         <div id="cookieHint">
		<jdoc:include type="modules" name="cookiehint" />
    </div>
	<script type="text/javascript">
	if(document.cookie.indexOf('hideCookieHint=1') != -1){
	    jQuery('#cookieHint').hide();
	}
	</script>
        
        
</body>
</html>
