<?php

function MD5_DIR($dir)
{
    if (!is_dir($dir))
    {
        return false;
    }
   
    $filemd5s = array();
    $d = dir($dir);

    while (false !== ($entry = $d->read()))
    {
        if ($entry != '.' && $entry != '..')
        {
             if (is_dir($dir.'/'.$entry))
             {
                 $filemd5s[] = MD5_DIR($dir.'/'.$entry);
             }
             else
             {
                 $filemd5s[] = md5_file($dir.'/'.$entry);
             }
         }
    }
    $d->close();
    return md5(implode('', $filemd5s));
}

$dirs = array(
	0 => "/templates/custom",
	1 => "/modules/mod_gu_slider_plus",
	2 => "/modules/mod_gu_tarifrechner2",
	3 => "/modules/mod_gu_teaser",
	4 => "/modules/mod_gu_teaser_plus"
);

$arr = array();

foreach ($dirs as $k => $v) {
	$arr[] = array('host' => $_SERVER['HTTP_HOST'], 'path' => $dirs[$k], 'hash' => MD5_DIR($_SERVER['DOCUMENT_ROOT'] . $dirs[$k]));
}

echo json_encode($arr);

?>