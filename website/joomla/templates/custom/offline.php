<?php
/**
 * @package     Joomla.Site
 * @subpackage  Template.system
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
$app = JFactory::getApplication();

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');

require_once JPATH_ADMINISTRATOR . '/components/com_users/helpers/users.php';

$twofactormethods = UsersHelper::getTwoFactorMethods();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="index, follow" />
<meta name="keywords" content="swp, stadtwerk, e24, erdgas, gas, strom, wasserkraft, regenerativ, energie, regenerative energie, günstig, direkt" />
<meta name="description" content="Hier entsteht in Kürze der Internetauftritt von E24" />	
<meta name="rights" content="SWP Stadtwerke Pforzheim GmbH & Co. KG" />
<meta name="copyright" content="GU KOMMUNIKATION, Gordana Uzelac, Agentur für Kommunikation" />
<meta name="viewport" content="width=device-width" />
<title>E24 | Ihre Experten für Energie</title>
<link rel="stylesheet" href="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/css/layout.css" type="text/css" media="all"  />
<link rel="stylesheet" href="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/css/styles.css" type="text/css" media="all"  />
<link rel="stylesheet" href="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/css/custom.css" type="text/css" media="all"  />
<link rel="stylesheet" href="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/css/offline.css" type="text/css" media="all"  />
<link href="images/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
<!--[if lte IE 6]>
	<link rel="stylesheet" href="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/css/layoutIE6.css" type="text/css" media="all"  />
	<script type="text/javascript" src="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/js/iepngfix_tilebg.js"></script>
	<style>img,div{behavior:url("<?php echo $this->baseurl . '/templates/' . $this->template; ?>/css/iepngfix.htc");}</style>
<![endif]-->
<script src="/media/jui/js/jquery.min.js" type="text/javascript"></script>
<script src="/media/jui/js/jquery-noconflict.js" type="text/javascript"></script>
<script src="/media/jui/js/jquery-migrate.min.js" type="text/javascript"></script>
<script src="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/js/jquery.lightbox_me.js" type="text/javascript"></script>
</head>
<body>
    <div id="wrapper">
        <div id="top">
                <div id="logo">
                        <img src="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/images/logo-energieservice24.png" title="" alt="Experten Energie" />
                </div>
                <div id="claim">
                        <span class="claim1">CLAIM 1</span> <span class="claim2">CLAIM 2</span>
                </div>
        </div>
        <div id="visual"> </div>
        <div id="content">
                <div id="content-inner">
                        <div class="box1-wrapper">
                                <div class="box1-header">
                                        <div class="colorplane"></div>
                                        <div id="header">Bald online</div>
                                </div>
                                <div class="box1">
                                        <h2>Hier entsteht in Kürze unser Internetauftritt</h2>
                                        <p>Neugierig? Besuchen Sie uns einfach bald wieder, um mehr über Energieservice24 zu erfahren.</p>
                                        <p>Mit Energieservice24 entscheiden Sie sich für reinen Ökostrom aus 100 % Wasserkraft und umweltschonendes Erdgas.</p>
                                        <p class="pad1"><a class="extern-link" href="mailto:kundenservice@energieservice24.de">Jetzt informieren<br />Kontaktanfrage senden</a></p>
                                </div>
                        </div>
                        <div class="box2">
                                <p>Energieservice24<br />Postfach 10 16 40<br />75116 Pforzheim<br /><a href="mailto:kundenservice@energieservice24.de">kundenservice@energieservice24.de</a></p>
                                <p>Serviceline 0700 797 105 13*<br />Telefax 0700 797 290 00</p>
                                <p><span class="small phonecosts">*mindestens 6,3 Cent aus dem Netz der Telekom pro angefangene 30 Sek. Mobilfunk i.d.R. höher</span></p>
                        </div>
                        <div class="spacer1">&nbsp;</div>

                </div>
                
        </div>
        <div id="footer">
                        <div id="copyright">
                                <span>© <script type="text/javascript">document.write(new Date().getFullYear()+".");</script> Alle Rechte vorbehalten. Energieservice24 ist ein Produkt der <a href="http://www.stadtwerke-pforzheim.de ">SWP Stadtwerke Pforzheim GmbH & Co. KG</a></span>
                        </div>
                        <div id="impressum">
                                <span>
                                        <a href="#" id="showlogin" onclick="javascript: return true;">Intern</a>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="http://www.stadtwerke-pforzheim.de/impressum">Impressum</a></span>
                        </div>
                </div>

                <?php include_once JPATH_THEMES.'/'.$this->template.'/includes/template_offline_login.php'; ?>
    </div>

<script type="text/javascript">	
jQuery(document).ready(function($){
	$('#showlogin').click(function(e) {
		$("#offlinelogin").lightbox_me({
			centered: true,
			preventScroll: true
		});
		e.preventDefault();
	});
});	    
</script>
</body>
</html>