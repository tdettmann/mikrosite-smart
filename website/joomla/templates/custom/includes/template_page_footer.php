<?php
/* 
 * Fuss-Bereich des Templates. Footer Menü Abbinder
 */
?>
<div id="navigator-row">
	<div id="navigator" class="container" tabindex="1">
		<div id="navigator-inner" class="row">
			<jdoc:include type="modules" name="navigator" />
			<div class="clear"></div>
		</div>
	</div>
</div>
<div id="footer">
	<div class="container">
		<!--
		<div id="navigatorshow">
			<a class="shownavigator" href="#shownavigator">&nbsp;</a>
		</div>
		<div id="footermenu">
			<jdoc:include type="modules" name="footermenu" />
		</div>
		!-->
		<div id="abbinder" >
			
			<?php 
				$abbinder = $this->params->get('abbinder');
				$abbinder2 = JText::_('COM_FOOTER_ABBINDER_TEXT');
				if (!empty($abbinder) && !empty($abbinder2)) {
				    $abbinder .= ' ';
				}
				$abbinder = str_replace('{JAHR}', date('Y') , $abbinder.$abbinder2);
				echo $abbinder;
			?>

		</div>
		<div class="clear"></div>
	</div>
</div>

