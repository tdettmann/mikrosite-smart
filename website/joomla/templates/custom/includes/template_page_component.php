<?php
/* 
 * Content Bereich.
 * Aufbau und Inhalt wie template_page_content.php
 * Angepasst zur reinen Komponenten ausgabe.
 */
?>
<div id="page-content">
	<div class="row">
		<div class="col-md-12">
			<div id="breadcrumbs"><jdoc:include type="modules" name="breadcrumbs" /></div>
			<jdoc:include type="component" />
			<div class="clear"></div>
		</div>
	</div>
</div>