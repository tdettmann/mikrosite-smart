<?php
/* 
 * Component View.
 * Einfaches Layout z.B. Bei Darstellung in Iframes
 * and open the template in the editor.
 */
// Add JavaScript Frameworks
//JHtml::_('behavior.framework');
?><!DOCTYPE html>
<html xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>">
<head>
	<?php include_once JPATH_THEMES.'/'.$this->template.'/includes/template_head.php'; ?>
</head>
<body class="contentpane">
    <div id="all">
	<div id="main">
	    <div id="cms-message"><jdoc:include type="message" /></div>
	    <div id="component"><jdoc:include type="component" /></div>
	</div>
    </div>
</body>
</html>

