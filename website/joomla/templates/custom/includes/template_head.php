<?php
/**
 * Inhalt des head-Tag
 */

// Remove RSForm front.css + responsive.css
$sheet = JURI::root(true) . '/media/com_rsform/css/front.css';
$sheet2 = JURI::root(true) . '/media/com_rsform/css/frameworks/responsive/responsive.css';
$document = &JFactory::getDocument();
if (array_key_exists($sheet, $document->_styleSheets)) unset($document->_styleSheets[$sheet]);
if (array_key_exists($sheet2, $document->_styleSheets)) unset($document->_styleSheets[$sheet2]);
?>
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1.0, user-scalable=no" name="viewport">
	<jdoc:include type="head" />
	<script src="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/js/template.js" type="text/javascript"></script>
	<!--[if lte IE 8]>
		<link rel="stylesheet" href="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/css/ie8.css" type="text/css" media="all" />
	<![endif]-->
	<!--[if lte IE 7]>
		<link rel="stylesheet" href="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/css/ie7.css" type="text/css" media="all" />
		<style type="text/css" media="screen">
			body {behavior: url(<?php echo $this->baseurl . '/templates/' . $this->template; ?>/js/cssHoverFix.htc); font-size: 100%;}
		</style>
		<script src="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/js/icomoon-lte-ie7.js" type="text/javascript"></script>
	<![endif]-->
	<!--[if lte IE 9]>
		<style>
			select{
				background-image: none;
				padding-right: 5px;
			}
		</style>
	<script src="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/js/jquery.placeholder.js" type="text/javascript"></script>
	<![endif]-->
