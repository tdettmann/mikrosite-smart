<?php
/* 
 * Hinweis auf veralteten Browser mit Empfehlungen und Links zu den Browser Herstellern.
 */
?>
<div id="oldbrowserhint" style="margin:5px auto; text-align:left; width:780px; border: 1px solid #2F2F2F; padding: 10px;">
<h2 style="color: #2F2F2F;">Ihr Webbrowser ist veraltet und nicht mehr sicher!</h2>
<p style="color: #2F2F2F; font-size: 12px;">Viele Webseiten ﻿unterstützen veraltete Browser nicht mehr und können 
	nur mit einem aktuellen Browser verwenden werden. So wird das Risiko reduziert, dass sich Ihr Computer mit Viren, 
	Trojanern und anderer Schadsoftware infiziert. Zu Ihrer Sicherheit, sollten Sie Ihren Browser immer aktuell halten. 
	Über die folgenden Links können Sie sich die jeweils aktuellste Browser-Version verschiedener Hersteller für Ihren 
	Computer herunter laden.</p>
<table cellpadding="2" cellspacing="2" width="100%">
<tbody>
<tr>
	<td><a style="color: #2F2F2F; font-size: 12px;" href="http://www.mozilla-europe.org/de/firefox/"><img src="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/images/mozilla_logo.png" title="Mozilla Firefox" alt="Mozilla Firefox" border="0" /></a></td>
	<td><a style="color: #2F2F2F; font-size: 12px;" href="http://www.google.de/chrome/"><img src="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/images/chrome_logo.png" title="Google Chrome" alt="Google Chrome" border="0" /></a></td>
	<td><a style="color: #2F2F2F; font-size: 12px;" href="http://www.opera.com/browser/"><img src="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/images/opera_logo.png" title="Opera Webbrowser" alt="Opera Webbrowser" border="0" /></a></td>
	<td><a style="color: #2F2F2F; font-size: 12px;" href="http://windows.microsoft.com/de-de/windows/upgrade-your-browser"><img src="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/images/ie_logo.png" title="Microsoft Windows Internet Explorer" alt="Microsoft Windows Internet Explorer" border="0" /></a></td>
</tr>
<tr>
	<td><a style="color: #2F2F2F; font-size: 12px;" href="http://www.mozilla-europe.org/de/firefox/">Mozilla Firefox<br />Mozilla Foundation, USA</td>
	<td><a style="color: #2F2F2F; font-size: 12px;" href="http://www.google.de/chrome/">Google Chrome<br />Google Inc., USA.</td>
	<td><a style="color: #2F2F2F; font-size: 12px;" href="http://www.opera.com/browser/">Opera<br />Opera Software ASA, Norwegen.</td>
	<td><a style="color: #2F2F2F; font-size: 12px;" href="http://windows.microsoft.com/de-de/windows/upgrade-your-browser">Internet Explorer<br />Microsoft Corporation, USA.</td>
</tr>
</tbody>
</table>
</div>