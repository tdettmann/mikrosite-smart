<?php
/* 
 * Content Bereich.
 */
?>
<div id="page-content">
	<div>
		<div class="container-fluid">
			<div class="row">
				<div>
				<?php  
					// Bühne
					include_once JPATH_THEMES.'/'.$this->template.'/includes/template_page_stage.php';
				?>
				</div>
			</div>
		</div>
	</div>
	<?php if($isHomeLayout): 
	/*----------------  Home Seite ------------------------------------------- */
	?>
		<?php  	// TODO
				if ($this->countModules( 'breadcrumbs' )) : ?>
		<div id="breadcrumbs">
			<div class="grid-auto"><jdoc:include type="modules" name="breadcrumbs" /></div>
		</div>
		<?php endif; ?>
		<div class="content">
			<?php 
			// *** Non-Fluid 1 ***
			if ($this->countModules( 'teaser1' )) : ?>
			<div id="<?php echo $this->params->get('section1','section1'); ?>" class="section">
				<div class="container">
					<div class="row">
						<jdoc:include type="modules" name="teaser1" />
					</div>
				</div>
			</div>
			<?php endif; ?>
			<?php 
			// *** Fluid 1 ***
			if ($this->countModules( 'teaser1-fluid' )) : ?>
			<div id="<?php echo $this->params->get('section1-fluid','section1-fluid'); ?>" class="section">
				<div class="container-fluid">
					<div class="row">
						<jdoc:include type="modules" name="teaser1-fluid" />
					</div>
				</div>
			</div>
			<?php endif; ?>
			<?php 
			// *** Non-Fluid 2 ***
			if ($this->countModules( 'teaser2' )) : ?>
			<div id="<?php echo $this->params->get('section2','section2'); ?>" class="section">
				<div class="container">
					<div class="row">
						<jdoc:include type="modules" name="teaser2" />
					</div>
				</div>
			</div>
			<?php endif; ?>
			<?php 
			// *** Fluid 2 ***
			if ($this->countModules( 'teaser2-fluid' )) : ?>
			<div id="<?php echo $this->params->get('section2-fluid','section2-fluid'); ?>" class="section">
				<div class="container-fluid">
					<div class="row">
						<jdoc:include type="modules" name="teaser2-fluid" />
					</div>
				</div>
			</div>
			<?php endif; ?>
			<?php 
			// *** Non-Fluid 3 ***
			if ($this->countModules( 'teaser3' )) : ?>
			<div id="<?php echo $this->params->get('section3','section3'); ?>" class="section">
				<div class="container">
					<div class="row">
						<jdoc:include type="modules" name="teaser3" />
					</div>
				</div>
			</div>
			<?php endif; ?>
			<?php 
			// *** Fluid 3 ***
			if ($this->countModules( 'teaser3-fluid' )) : ?>
			<div id="<?php echo $this->params->get('section3-fluid','section3-fluid'); ?>" class="section">
				<div class="container-fluid">
					<div class="row">
						<jdoc:include type="modules" name="teaser3-fluid" />
					</div>
				</div>
			</div>
			<?php endif; ?>
			<?php 
			// *** Non-Fluid 4 ***
			if ($this->countModules( 'teaser4' )) : ?>
			<div id="<?php echo $this->params->get('section4','section4'); ?>" class="section">
				<div class="container">
					<div class="row">
						<jdoc:include type="modules" name="teaser4" />
					</div>
				</div>
			</div>
			<?php endif; ?>
			<?php 
			// *** Fluid 4 ***
			if ($this->countModules( 'teaser4-fluid' )) : ?>
			<div id="<?php echo $this->params->get('section4-fluid','section4-fluid'); ?>" class="section">
				<div class="container-fluid">
					<div class="row">
						<jdoc:include type="modules" name="teaser4-fluid" />
					</div>
				</div>
			</div>
			<?php endif; ?>
			<?php 
			// *** Non-Fluid 5 ***
			if ($this->countModules( 'teaser5' )) : ?>
			<div id="<?php echo $this->params->get('section5','section5'); ?>" class="section">
				<div class="container">
					<div class="row">
						<jdoc:include type="modules" name="teaser5" />
					</div>
				</div>
			</div>
			<?php endif; ?>
			<?php 
			// *** Fluid 5 ***
			if ($this->countModules( 'teaser5-fluid' )) : ?>
			<div id="<?php echo $this->params->get('section5-fluid','section5-fluid'); ?>" class="section">
				<div class="container-fluid">
					<div class="row">
						<jdoc:include type="modules" name="teaser5-fluid" />
					</div>
				</div>
			</div>
			<?php endif; ?>
			<?php 
			// *** Non-Fluid 6 ***
			if ($this->countModules( 'teaser6' )) : ?>
			<div id="<?php echo $this->params->get('section6','section6'); ?>" class="section">
				<div class="container">
					<div class="row">
						<jdoc:include type="modules" name="teaser6" />
					</div>
				</div>
			</div>
			<?php endif; ?>
			<?php 
			// *** Fluid 6 ***
			if ($this->countModules( 'teaser6-fluid' )) : ?>
			<div id="<?php echo $this->params->get('section6-fluid','section6-fluid'); ?>" class="section">
				<div class="container-fluid">
					<div class="row">
						<jdoc:include type="modules" name="teaser6-fluid" />
					</div>
				</div>
			</div>
			<?php endif; ?>
			<?php 
			// *** Non-Fluid 7 ***
			if ($this->countModules( 'teaser7' )) : ?>
			<div id="<?php echo $this->params->get('section7','section7'); ?>" class="section">
				<div class="container">
					<div class="row">
						<jdoc:include type="modules" name="teaser7" />
					</div>
				</div>
			</div>
			<?php endif; ?>
			<?php 
			// *** Fluid 7 ***
			if ($this->countModules( 'teaser7-fluid' )) : ?>
			<div id="<?php echo $this->params->get('section7-fluid','section7-fluid'); ?>" class="section">
				<div class="container-fluid">
					<div class="row">
						<jdoc:include type="modules" name="teaser7-fluid" />
					</div>
				</div>
			</div>
			<?php endif; ?>
			<span class="fahrstuhl hidden-xs"><img id="scrollhintimg" src="images/layout/pfeil-footer-menue.png" /></span>
		</div>
	<?php elseif($isFullsizeLayout): 
	/*---------------- Layout Ganze Breite ----------------------------------- */
	?>
		<div class="container">
			<div id="breadcrumbs">
				<jdoc:include type="modules" name="breadcrumbs" />
			</div>
			<div class="content row">
				<div class="col-sm-12">
					<jdoc:include type="component" />
					<div class="clearfix"></div>
					<div id="verwandtethemen"><jdoc:include type="modules" name="verwandtethemen" /></div>
				</div>
			</div>
		</div>
	<?php else: 
	/*---------------- Beitrags Layout --------------------------------------- */
	?>
		<div class="container">
			<div id="breadcrumbs">
				<div class="grid-auto"><jdoc:include type="modules" name="breadcrumbs" /></div>
			</div>
			<div class="content row">
				<div class="col-sm-8">
					<div class="content content-links">
					<jdoc:include type="component" />
					<div class="clearfix"></div>
					<div id="verwandtethemen"><jdoc:include type="modules" name="verwandtethemen" /></div>
					</div>
				</div>	
				<div class="col-sm-4 spalterechts content"><jdoc:include type="modules" name="spalterechts" /></div>
			</div>
		</div>
	<?php endif; ?>
	<?php if($this->countModules('pagefooter')>0): ?>
	<div id="page-footer">
		<div class="col-sm-12"><jdoc:include type="modules" name="pagefooter" /></div>
	</div>
	<?php endif; ?>
        <!-- -------------- Back To Top Button ------------------------------------ !-->
        <!-- <a href="#top" class="backToTop"><span>&nbsp;</span></a> !-->
</div>
