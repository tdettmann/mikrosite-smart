<?php
/* 
 * Modul Slider über dem Content.
 * Modulposition wird nur angezeigt wenn mindestens ein Modul vorhanden ist
 * - Bei einem Modul wird das Modul ausgegeben
 * - Bei mindestens 2 Module auf der Modulposition liegen, werden diese
 *   als Slider ausgegeben. 
 */
$countbuehne = $this->countModules('buehneslider');
 if($countbuehne>1):
//if(false):
?>
	<div id="buehne" class="slider">
		<jdoc:include type="modules" name="buehneslider" style="slickslider" />
	</div>
	<script type="text/javascript">
		(function($, undefined) {
			$(document).ready(function(){
				
				var slick = $('.slider').slick({
					dots: true,
					//arrows: false,
					infinite: true,	// Endlos Wiederholung
					speed: 1000,	// Animations Speed
					adaptiveHeight: true,
					slidesToShow: 1,
					autoplay: true,
					autoplaySpeed: 6000	// Verzögerung für Autoplay
				});
				
				// Stop slider, when out of view (fixing autoscrolling because of adaptive height)
				$(window).scroll(function() {			
					var autoplay_height = $("#buehne").offset().top + $("#buehne").height() - $("#header").height();
					if ($(window).scrollTop() > autoplay_height) {
						$('.slider').slick('slickPause');
					} else {
						$('.slider').slick('slickPlay');						
					}
				});
			});
		}(jQuery));
	</script>
	
	<?php /*
        <script type="text/javascript">
		jQuery(document).ready(function($) {
			jQuery('#buehne').bjqs({
				'height' : 300,
				'width' : 980,
				'responsive' : true,
				'showcontrols': true,
				'hoverpause' : true,
				'showmarkers' : true,
				'centermarkers' : true,
				'automatic' : <?php echo $this->params->get('modulslider_automatic', 'false' );?>, // automatic
				'animduration' : <?php echo $this->params->get('modulslider_animduration', 750 );?>, // how fast the animation are
				'animspeed' : <?php echo $this->params->get('modulslider_animspeed', 5000 );?> // the delay between each slide
			});
			//jQuery(".bjqs-controls").css('display','none');
			jQuery(".bjqs-slide").touchwipe({
				 wipeLeft: function() { jQuery('.bjqs-prev a').trigger('click'); },
				 wipeRight: function() { jQuery('.bjqs-next a').trigger('click'); },
				 min_move_x: 20,
				 min_move_y: 20,
				 preventDefaultEvents: true
			});
		});
	</script>
	*/ ?>
<?php elseif ($countbuehne>0): ?>
	<div id="buehne" class=""><jdoc:include type="modules" name="buehneslider" /></div>
<?php else: ?>
	<div id="buehne" class=""><jdoc:include type="modules" name="buehnestandard" /></div>
<?php endif;