<?php
/* 
 * Kopfbereich des Templates
 * z.B. mit Logo und Hauptmenü
 */
?>
<div id="header">
    <div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div id="logo"><a href="/">
					<img src="<?php echo $this->baseurl . '/' . htmlspecialchars($this->params->get('logo')); ?>" 
						 alt="<?php echo htmlspecialchars($config->get('sitename'));?>" /></a>
				</div>
				<?php if ($this->params->get('hotline')): ?>
					<div id="menu-telefon" itemscope itemtype="http://schema.org/LocalBusiness"><span class="label">Telefon</span><span class="icon-telefon" itemprop="telephone"><?php echo $this->params->get('hotline',''); ?></span></div>
				<?php endif; ?>			
				<div id="search"><jdoc:include type="modules" name="suche" /></div>
				<nav id="hauptnavigation">
						<div id="desktopmenu" class="hauptnav">
							<jdoc:include type="modules" name="menu-main" />
						</div>
						<!-- Responsive !-->
						<div id="mobilmainnav">
							<ul>
								<li><a href="#menu"><?php echo JText::_('COM_CONTENT_MENU'); ?></a></li>
								<li id="mobil-menu-suche"><a href="/suche"><img src="/images/layout/lupe.png" /></a></li>
								<li id="mobil-menu-telefon"><a href="tel:<?php echo $this->params->get('hotline',''); ?>"><img src="/images/layout/telefon.svg" /></a></li>
							</ul>
						</div>
						<div id="mobilnavigation" class="hauptnav">
							<jdoc:include type="modules" name="menu-main" />
						</div>
				</nav>
			</div>
		</div>

		<?php if($this->countModules('actionicons')>0): ?>
		<div id="actionicons-row">
			<div id="stickyactionicons" class="stick">
					<div id="actionicons"><jdoc:include type="modules" name="actionicons" /></div>
			</div>
		</div>
		<?php endif; ?>
    </div>
</div>
