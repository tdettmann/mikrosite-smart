<?php
/**
 * Joomla-Template Footer Javascripts.
 * 
 * @version	1.0
 * @package	Joomla.Site
 * @subpackage	Templates
 * @author      GU KOMMUNIKATION Karlsruhe, Thomas Langer (TL)
 * @copyright	Copyright (C) 2013 Gordana Uzelac, Agentur für Kommunikation - GU KOMMUNIKATION, Karlsruhe.
 * 
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>
<script type="text/javascript"><!--//--><![CDATA[//><!--
// für Bookmark Funktion function bookmarkMe()
var pageUrl = location.href;
var pageTitle = '<?php echo $doc->getTitle(); ?>';
var pageComment = '';

<?php if (!constant('ISMOBIL')) : ?>
(function($, undefined ) {
	$(document).ready(function(){
		$('.fahrstuhl').addClass('show');
		
		var myTargetHref = $(location).attr('href');
		
		if (myTargetHref.lastIndexOf('/#') != -1) {
			
		    var myTarget = myTargetHref.substring( myTargetHref.lastIndexOf('#') );
			
			if ($(myTarget).length) {
				$('html, body').animate({
					scrollTop: $(myTarget).offset().top - $("#header").height() + 1
				}, 500);
		    }
		}	

		$('body').scrollspy({ target: '#hauptnavigation' });		
	});
}(jQuery));
<?php endif; ?>
//--><!]]></script>
<!--[if lte IE 9]>
	<script>
	jQuery(function() {
		jQuery('input, textarea').placeholder();
	});
	</script>
<![endif]-->