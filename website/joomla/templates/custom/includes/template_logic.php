<?php

/**
 * Joomla-Template Logik.
 * 
 * @version	1.0
 * @package	Joomla.Site
 * @subpackage	Templates.swp
 * @author      GU KOMMUNIKATION Karlsruhe
 * @copyright	Copyright (C) 2013 Gordana Uzelac, Agentur für Kommunikation - GU KOMMUNIKATION, Karlsruhe.
 * 
 */
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// Initialisierung
$app    = JFactory::getApplication();
$config = JFactory::getConfig();
$doc    = JFactory::getDocument();
$lang   = JFactory::getLanguage();

// Browser-Erkennung
//http://api.joomla.org/Joomla-Platform/Application/JApplicationWebClient.html
$client 		= new JApplicationWebClient();

$templateparams	= $app->getTemplate(true)->params;
$sitename		= $app->getCfg('sitename');
$menu			= $app->getMenu();
$activmenu		= $app->getMenu()->getActive();
$pathway		= $app->getPathway();


// Variablen definieren und initialisieren

$params        = $app->getParams();
$headdata      = $doc->getHeadData();
$pageclass_sfx = '';
$templatepath = $this->baseurl.'/templates/'.$this->template;

$isMSIE6 = false;
$isOldBrowser = false;

// advanced parameter

  // disable js
  if ( $this->params->get('disablejs') ) {
    $fnjs=$this->params->get('fnjs');
    if (trim($fnjs) != '') {
      $filesjs=explode(',', $fnjs);
      $head = (array) $headdata['scripts'];
      $newhead = array();         
      foreach($head as $key => $elm) {
        $add = true;
        foreach ($filesjs as $dis) {
          if (strpos($key,$dis) !== false) {
            $add=false;
            break;
          } 
        }
        if ($add) $newhead[$key] = $elm;
      }
      $headdata['scripts'] = $newhead;
    } 
  } 
  // disable css
  if ( $this->params->get('disablecss') ) {
    $fncss=$this->params->get('fncss');
    if (trim($fncss) != '') {
      $filescss=explode(',', $fncss);
      $head = (array) $headdata['styleSheets'];
      $newhead = array();         
      foreach($head as $key => $elm) {
        $add = true;
        foreach ($filescss as $dis) {
          if (strpos($key,$dis) !== false) {
            $add=false;
            break;
          } 
        }
        if ($add) $newhead[$key] = $elm;
      }
      $headdata['styleSheets'] = $newhead;
    } 
  }
  $doc->setHeadData($headdata); 

// Joomla Generator tag entfernen
$this->setGenerator(null);

// force latest IE & chrome frame
$doc->setMetadata('X-UA-Compatible', 'IE=edge,chrome=1');

jimport('joomla.environment.browser');
$browser = JBrowser::getInstance();
$browserType = $browser->getBrowser();
$browserVersion = $browser->getMajor();

// Alter Browser / Microsoft Internet Explorer 6
// Microsoft Internet Explorer 6
if (($browserType == 'msie') && ($browserVersion < 7)) {
	$isMSIE6 = true;
	$isOldBrowser = true;
}

$pageclass_sfx = $params->get('pageclass_sfx','');
$activmenuclass='';
if ($activmenu) {
	$pageclass_sfx=$activmenu->params->get('pageclass_sfx');
}

// Seitentyperkennung
// Startseite erkennen
$isDefaultMenu	= ($activmenu == $menu->getDefault($lang->getTag())); // true = Frontside oder nicht im Menü verknüpfte Beiträge
$isFrontside	= ($isDefaultMenu && !count($pathway->getPathWay())); // Menüpfadlänge == 0 wird durch $isDefaultMenu impliziert

// Seiten-layout ermitteln
$homelayout      =  strpos($pageclass_sfx, 'homelayout');
$isHomeLayout	= (((!$isDefaultMenu && end($pathway->getPathWay())->link) || $isFrontside) && ($homelayout !== false)); //$activmenuclass == 'boxlayout'));
$isFullsizeLayout =  ( strpos($pageclass_sfx, 'fullsizelayout')!== false);


// CSS-Dateien hinzufügen
$doc->addStyleSheet($templatepath . '/css/template.css', 'text/css', 'all');
$doc->addStyleSheet($templatepath . '/slick/slick.css', 'text/css', 'all');

// unset Komponenten-Style
unset($this->_styleSheets[JURI::base(true).'/components/com_rsform/assets/css/front.css']);

if($isMSIE6) {
	// Internet Explorer 6. jQuery entfernen um die Seite anzeigbar zu machen
	unset($this->_scripts[JURI::root(true).'/media/jui/js/jquery.min.js']);
}

// Konstanten definieren
define('ISMOBIL', $client->mobile);
define('ISMSIE6', $isMSIE6 );