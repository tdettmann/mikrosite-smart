<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$headerTag      = htmlspecialchars($params->get('header_tag', 'h3'));
$bootstrapSize  = (int) $params->get('bootstrap_size', 0);

$moduleClass    = $bootstrapSize != 0 ? ' col-sm-' . $bootstrapSize : 'col-sm-12';

$nomobile = strpos($params->get('moduleclass_sfx'), 'no-mobile');
$nodesktop= strpos($params->get('moduleclass_sfx'), 'no-desktop');

// Mobil-Gerät und Box soll Mobil nicht angezeigt werden -> Keine Ausgabe
if (constant('ISMOBIL') && $nomobile!==false) {
	return false;
}

// Kein Mobil-Gerät -> Desktop und Box soll auf den Desktop nicht angezeigt werden -> Keine Ausgabe
if (!constant('ISMOBIL') && $nodesktop!==false) {
	return false;
}

?>
<div class="<?php echo $moduleClass . ' custom zitat' . $moduleclass_sfx ;  ?>" <?php if ($params->get('backgroundimage')) : ?> style="background-image:url(<?php echo $params->get('backgroundimage');?>)"<?php endif;?> >
	<blockquote>
		<?php echo $module->content; ?>
	</blockquote>
</div>
