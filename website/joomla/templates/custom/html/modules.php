<?php
/**
 * Joomla-Template für die Stadtwerke Pforzheim.
 * 
 * @version	3.3
 * @package	Joomla.Site
 * @subpackage	modChrome
 * @author      GU KOMMUNIKATION Karlsruhe, Thomas Langer (TL)
 * @copyright	Copyright (C) 2015 Gordana Uzelac, Agentur für Kommunikation - GU KOMMUNIKATION, Karlsruhe.
 * @todo        Code vollständig dokumentieren, print.css hinzufügen
 * 
 */

function modChrome_box ($module, &$params, &$attribs)
{
    $moduleTag      = $params->get('module_tag', 'div');
    $headerTag      = htmlspecialchars($params->get('header_tag', 'h3'));
    $bootstrapSize  = (int) $params->get('bootstrap_size', 0);
    $moduleClass    = $bootstrapSize != 0 ? ' span' . $bootstrapSize : '';
    if (!empty ($module->content)) :
	$style=$params->get('inlinestyle');
	if ($style) {
	    if(strpos($style,'style')=== false) { 
		$style=' style="'.$style.'"'; 
	    }
	}
	$nomobile = strpos($params->get('moduleclass_sfx'), 'no-mobile');
	$nodesktop= strpos($params->get('moduleclass_sfx'), 'no-desktop');
	$mobil=false;
	if (defined( 'ISMOBIL' )) { $mobil=ISMOBIL; }

	// Mobil-Gerät und Box soll Mobil nicht angezeigt werden -> Keine Ausgabe
	if ($mobil && !($nomobile===false)) {
		return false;
	}

	// Kein Mobil-Gerät -> Desktop und Box soll auf den Desktop nicht angezeigt werden -> Keine Ausgabe
	if (!$mobil && !($nodesktop===false)) {
		return false;
	}
    ?>
	<div class="box <?php echo $params->get('moduleclass_sfx'); ?><?php echo $moduleClass; ?>" <?php echo $style; ?>>
	    <div class="modulcontent">
		<?php if ((bool) $module->showtitle) :?>
		    <<?php echo $headerTag; ?> class="<?php echo $params->get('header_class'); ?>"><?php echo $module->title; ?></<?php echo $headerTag; ?>>
		<?php endif; ?>
		<?php echo $module->content; ?>
	    </div>
	</div>
    <?php endif;
}

/*
function modChrome_bxslider($module, &$params, &$attribs)
{
    $moduleTag      = $params->get('module_tag', 'div');
    $headerTag      = htmlspecialchars($params->get('header_tag', 'h3'));
    $bootstrapSize  = (int) $params->get('bootstrap_size', 0);
    $moduleClass    = $bootstrapSize != 0 ? ' span' . $bootstrapSize : '';
    if (!empty ($module->content)) :
	$style=$params->get('inlinestyle');
	if ($style) {
	    if(strpos($style,'style')=== false) { 
		$style=' style="'.$style.'"'; 
	    }
	}
	$nomobile = strpos($params->get('moduleclass_sfx'), 'no-mobile');
	$nodesktop= strpos($params->get('moduleclass_sfx'), 'no-desktop');
	$mobil=false;
	if (defined( 'ISMOBIL' )) { $mobil=ISMOBIL; }

	// Mobil-Gerät und Box soll Mobil nicht angezeigt werden -> Keine Ausgabe
	if ($mobil && !($nomobile===false)) {
		return false;
	}

	// Kein Mobil-Gerät -> Desktop und Box soll auf den Desktop nicht angezeigt werden -> Keine Ausgabe
	if (!$mobil && !($nodesktop===false)) {
		return false;
	}
    ?>
	<li>
		<?php echo $module->content; ?>
	</li>
    <?php endif;
}
*/

function modChrome_slickslider($module, &$params, &$attribs) 
{	
	// TODO: Add active class for the first slider.
?><div class="<?php echo $params->get('moduleclass_sfx'); ?>">
	<?php echo $module->content; ?>
</div><?php 
}

function modChrome_module($module, &$params, &$attribs)
{
	//----------------------------
	$nomobile = strpos($params->get('moduleclass_sfx'), 'no-mobile');
	$nodesktop= strpos($params->get('moduleclass_sfx'), 'no-desktop');

	// Mobil-Gerät und Box soll Mobil nicht angezeigt werden -> Keine Ausgabe
	if (constant('ISMOBIL') && $nomobile!==false) {
		return false;
	}

	// Kein Mobil-Gerät -> Desktop und Box soll auf den Desktop nicht angezeigt werden -> Keine Ausgabe
	if (!constant('ISMOBIL') && $nodesktop!==false) {
		return false;
	}

	//----------------------------
	
	$headerTag      = htmlspecialchars($params->get('header_tag', 'h3'));
	$moduleTag      = htmlspecialchars($params->get('module_tag', 'div'));
	$bootstrapSize  = (int) $params->get('bootstrap_size', 0);
	$moduleClass    = $bootstrapSize != 0 ? 'col-sm-' . $bootstrapSize : 'col-sm-12';
	
	?>

	<<?php echo $moduleTag; ?> class="<?php echo $moduleClass . ' custom' . $moduleclass_sfx ;  ?>" <?php if ($params->get('backgroundimage')) : ?> style="background-image:url(<?php echo $params->get('backgroundimage');?>)"<?php endif;?> >
		<?php if ((bool) $module->showtitle) :?>
			<<?php echo $headerTag; ?> class="<?php echo $params->get('header_class'); ?>"><?php echo $module->title; ?></<?php echo $headerTag; ?>>
		<?php endif; ?>
		<?php echo $module->content; ?>
	</<?php echo $moduleTag; ?>>

    <?php 
}
