<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_menu
 * @author		Friedrich Schroedter
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Note. It is important to remove spaces between elements.
?>
<?php // The menu class is deprecated. Use nav instead. 
//echo "------------------------------------------";
//print_r($list);
//echo "------------------------------------------";

$headingLevel = 0;
?>

<ul class="nav menu-level-0 menu<?php echo $class_sfx;?>"<?php
	$tag = '';
	if ($params->get('tag_id') != null)
	{
		$tag = $params->get('tag_id').'';
		echo ' id="'.$tag.'"';
	}
?>>
<?php
reset($list);
$firstItem = current($list);
$rootLevel = $firstItem->level - 1;

foreach ($list as $i => &$item) :
	$class = 'item-'.$item->id;
	if ($item->id == $active_id)
	{
		$class .= ' current';
	}

	if (in_array($item->id, $path))
	{
		$class .= ' active';
	}
	elseif ($item->type == 'alias')
	{
		$aliasToId = $item->params->get('aliasoptions');
		if (count($path) > 0 && $aliasToId == $path[count($path) - 1])
		{
			$class .= ' active';
		}
		elseif (in_array($aliasToId, $path))
		{
			$class .= ' alias-parent-active';
		}
	}

	if ($item->type == 'separator')
	{
		$class .= ' divider';
	}

	if ($item->deeper)
	{
		$class .= ' deeper';
	}

	if ($item->parent)
	{
		$class .= ' parent';
	}

	if (!empty($class))
	{
		$class = ' class="'.trim($class) .'"';
	}
	
	$level = $item->level - $rootLevel;	
	//--------------------
	if ( $item->deeper ||
		($item->type == 'heading')
	)
	{
		if ($headingLevel) {
			echo '</ul></li>';
			$headingLevel--;
		}
	}
	//--------------------
	
	echo '<li'.$class.' onmouseover="">';// Fix for IOS add: onmouseover=""

	// Render the menu item.
	switch ($item->type) :
		case 'separator':
		case 'url':
		case 'component':
		case 'heading':

			$classArray = explode(' ', $item->anchor_css);
			if (in_array('nolink', $classArray)) {
				require JModuleHelper::getLayoutPath('mod_menu', 'default_heading');
				break;
			} 
			
			require JModuleHelper::getLayoutPath('mod_menu', 'default_'.$item->type);
			break;

		default:
			$classArray = explode(' ', $item->anchor_css);
			if (in_array('nolink', $classArray)) {
				require JModuleHelper::getLayoutPath('mod_menu', 'default_heading');
				break;
			}
			require JModuleHelper::getLayoutPath('mod_menu', 'default_url');
			break;
	endswitch;

	// The next item is deeper.
	if ($item->deeper)
	{
		echo '<ul class="nav-child ' . $item->anchor_css . ' menu-level-'.$level.'">';
	}
	// The next item is shallower.
	elseif ($item->shallower)
	{
		echo '</li>';
		echo str_repeat('</ul></li>', $item->level_diff);
	}
	// The next item is on the same level.
	elseif ($item->type == 'heading' && $level > 1)
	{
		echo '<ul class="menu-heading menu-col menu-level-'.$level.'">';
		$headingLevel++;
	}
	else 
	{
		echo '</li>';
	}

	//--------------------
	if ($item->shallower)
	{
		if ($headingLevel) {
			echo '</ul></li>';
			$headingLevel--;
		}
	}
	//--------------------
	
endforeach;
?></ul>
