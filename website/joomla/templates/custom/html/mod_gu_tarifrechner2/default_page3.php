<?php
/**
 * Template für Joomla-Modul GU Tarifrechner 
 * Schritt / Page 3: Kontaktformular
 * 
 * @version	1.0.2
 * @package	Joomla.Site
 * @subpackage	mod_gu_tarifrechner
 * @author      Thomas Langer (TL)
 * @copyright	Copyright (C) 2014 Gordana Uzelac, Agentur für Kommunikation - GU KOMMUNIKATION, Karlsruhe.
 * 
 */
// no direct access
defined('_JEXEC') or die('Restricted access');
?>
<div class="page3">
	<form class="gu_tarif_kontaktform" autocomplete="off">
		<div class="kontaktcol">
			<input type="hidden" name="kundengruppe" value="" />
			<input type="hidden" name="activetarif" value="" />
			<input type="hidden" name="energieart" value="" />
			<input type="hidden" name="tarifplz" value="" />
			<input type="hidden" name="verbrauch" value="" />
			<input type="hidden" name="laufzeit" value="" />
			<input type="hidden" name="tarifname" value="" />
			<input type="hidden" name="tarifname2" value="" />
			<input type="hidden" name="netzanbieter" value="" />
			<input type="hidden" name="tarifgp" value="" />
			<input type="hidden" name="tarifap" value="" />
			<div class="gu_tarif_formrow gu_tarifrechner_anrede">
				<span class="gu_tarif_label"><?php echo JText::_('MOD_GU_TARIFRECHNER_ANREDE'); ?> <strong class="gu_tarif_required_anrede">*</strong></span>
				<input name="anrede" type="radio" value="1" required="required" />&nbsp;&nbsp;<label><?php echo JText::_('MOD_GU_TARIFRECHNER_ANREDE_FRAU'); ?></label>
				&nbsp;&nbsp;&nbsp;&nbsp;
				<input name="anrede" type="radio" value="2" required="required" />&nbsp;&nbsp;<label><?php echo JText::_('MOD_GU_TARIFRECHNER_ANREDE_HERR'); ?></label>
			</div>
			<div class="gu_tarif_formrow gu_tarifrechner_gewerbe">
				<label class="gu_tarif_label"><?php echo JText::_('MOD_GU_TARIFRECHNER_GEWERBE'); ?> <strong class="gu_tarif_required_firma"></strong></label>
				<input type="text" value="" size="20" name="firma" />
			</div>
			<div class="gu_tarif_formrow">
				<label class="gu_tarif_label"><?php echo JText::_('MOD_GU_TARIFRECHNER_VORNAME'); ?></label>
				<input type="text" value="" size="20" name="vorname" />
			</div>
			<div class="gu_tarif_formrow">
				<label class="gu_tarif_label"><?php echo JText::_('MOD_GU_TARIFRECHNER_NACHNAME'); ?> <strong class="gu_tarif_required_name">*</strong></label>
				<input type="text" value="" size="20" name="nachname" required="required" />
			</div>
			<div class="gu_tarif_formrow">
				<label class="gu_tarif_label"><?php echo JText::_('MOD_GU_TARIFRECHNER_STRASSE'); ?> <strong class="gu_tarif_required_strasse"></strong></label>
				<input type="text" value="" size="20" name="strasse" />
			</div>
			<div class="gu_tarif_formrow">
				<label class="gu_tarif_label"><?php echo JText::_('MOD_GU_TARIFRECHNER_PLZ'); ?> <strong class="gu_tarif_required_plz"></strong></label>
				<input type="text" value="" size="20"  name="plz" maxlength="5" onkeyup="this.value=this.value.replace(/\D/, '');" />
			</div>
			<div class="gu_tarif_formrow">
				<label class="gu_tarif_label"><?php echo JText::_('MOD_GU_TARIFRECHNER_ORT'); ?> <strong class="gu_tarif_required_ort"></strong></label>
				<input type="text" value="" size="20"  name="ort" />
			</div>
			<div class="gu_tarif_formrow">
				<label class="gu_tarif_label"><?php echo JText::_('MOD_GU_TARIFRECHNER_TELEFON'); ?> <strong class="gu_tarif_required_telefon"></strong></label>
				<input type="text" value="" size="20"  name="telefon" />
			</div>
			<div class="gu_tarif_formrow">
				<label class="gu_tarif_label"><?php echo JText::_('MOD_GU_TARIFRECHNER_TELEFAX'); ?></label>
				<input type="text" value="" size="20"  name="telefax" />
			</div>
			<div class="gu_tarif_formrow">
				<label class="gu_tarif_label"><?php echo JText::_('MOD_GU_TARIFRECHNER_EMAIL'); ?> <strong class="gu_tarif_required_email">*</strong></label>
				<input type="text" value="" size="20"  name="email" />
			</div>
			<div class="gu_tarif_formrow gu_tarifrechner_kontaktauf">
				<p><?php echo JText::_('MOD_GU_TARIFRECHNER_KONTAKTAUFNAME'); ?></p>
				<p>
					<input name="kontaktaufnahme" type="checkbox" value="R&uuml;ckruf" />&nbsp;&nbsp;<label><?php echo JText::_('MOD_GU_TARIFRECHNER_RUECKRUF'); ?></label><br />
					<input name="vertrags_unterlagen" type="checkbox" value="Ja" />&nbsp;&nbsp;<label><?php echo JText::_('MOD_GU_TARIFRECHNER_VERTRAGZUSENDE'); ?></label>
				</p>
			</div>
			<p>* <?php echo JText::_('MOD_GU_TARIFRECHNER_PFLICHTFELD'); ?></p>
		</div>
		<div class="formcol3">
			<div class="gu_tarif_formmsg"></div>
			<div class="gu_tarifspacer"></div>
			<div class="gu_tarifzusammenfassung gu_tarifmsg">&nbsp;</div>
			<div class="gu_tarifspacer"></div>
			<div>
				<input type="button" class="gu_tarif_button_anpassen gu_tarif_button submitbutton" value="<?php echo JText::_('MOD_GU_TARIFRECHNER_FORMSTART_BUTTON'); ?>" />
				<div class="gu_tarifspacer"></div>
				<input type="button" class="gu_tarif_button_anfragesenden gu_tarif_button submitbutton" value="<?php  echo $params->get('buttonkontaktsenden', JText::_('MOD_GU_TARIFRECHNER_FORMKONTAKT_BUTTON')); ?>" />
			</div>
		</div>
	</form>
</div>