<?php
/**
 * Template für Joomla-Modul GU Tarifrechner
 * Schritt / Page 1: Tarifformular
 * 
 * @version	   1.0.2
 * @package	    Joomla.Site
 * @subpackage	mod_gu_tarifrechner
 * @author      Thomas Langer (TL)
 * @copyright	Copyright (C) 2014 Gordana Uzelac, Agentur für Kommunikation - GU KOMMUNIKATION, Karlsruhe.
 * 
 */
// no direct access
defined('_JEXEC') or die('Restricted access');
?>
<div class="page1 activepage">
<?php // Ausgabe Formular Tarif 1
if ($params->get('tarif1online',0)): ?>
	<div class="tarif1" style="<?php if ($params->get('activetarif',1)!=1) { echo 'display: none;'; }; ?>">
	<form class="gu_tarif1form" autocomplete="off">
		<div class="form2col">
			<div class="formrow1">
				<input type="hidden" name="energieart" value="<?php echo $params->get('tarifart1',1); ?>" />
				<input type="hidden" name="activetarif" value="1" />
				<input type="text" name="tarifplz" value="" placeholder="<?php echo JText::_('MOD_GU_TARIFRECHNER_PLZ'); ?>" maxlength="5" onkeyup="this.value=this.value.replace(/\D/, '');" />
				<div class="gu_tarifspacer"></div>
				<?php echo modGuTarifrechner2Helper::getCustomerSelect($params->get('kundengruppe1',1)); ?>
			</div>
			<div class="formrow2">
				<input type="text" name="verbrauch" value="" placeholder="<?php echo JText::_('MOD_GU_TARIFRECHNER_JAHRESVERBRAUCH'); ?>" maxlength="6" onkeyup="this.value=this.value.replace(/\D/, '');" />
				<div class="gu_tarifspacer"></div>
				<select name="laufzeit" size="1" data-placeholder="<?php echo JText::_('MOD_GU_TARIFRECHNER_VERTRAGSLAUFZEIT'); ?>">
					
					 <option value="0" selected="selected" style="display:none;"><?php echo JText::_('MOD_GU_TARIFRECHNER_VERTRAGSLAUFZEIT'); ?></option>
					
					<?php // Ausgabe dropdown
						for ($vl= $params->get('minlaufzeit1',6); $vl<=$params->get('maxlaufzeit1',36); $vl++)
						{
							echo '<option value="'.$vl.'">'.$vl.' '.JText::_('MOD_GU_TARIFRECHNER_MONATE').'</option>';
						}
					?>
				</select>
			</div>
			<div class="clear"></div>
		</div>
		<div class="formcol3">
			<input type="button" data-tariftype="1" class="gu_tarif_tarif1abfragen gu_tarif_tarifabfragen" value="<?php echo $params->get('calltoaction',JText::_('MOD_GU_TARIFRECHNER_CALLTOACTION_BUTTON')); ?>" />
			<div class="gu_tarifspacer"></div>
			<div class="gu_tarif_formmsg"></div>
		</div>
		<div class="clear"></div>
	</form>
	</div>
<?php endif;
// Ausgabe Formular Tarif 3
if ($params->get('tarif3online',0)): ?>
	<div class="tarif3" style="<?php if ($params->get('activetarif',1)!=3) { echo 'display: none;'; }; ?>">
	<form class="gu_tarif3form" autocomplete="off">
		<div class="form2col">
			<div class="formrow1">
				<input type="hidden" name="energieart" value="<?php echo $params->get('tarifart3',1); ?>" />
				<input type="hidden" name="activetarif" value="3" />
				<input type="text" name="tarifplz" value="" placeholder="<?php echo JText::_('MOD_GU_TARIFRECHNER_PLZ'); ?>" maxlength="5" onkeyup="this.value=this.value.replace(/\D/, '');" />
				<div class="gu_tarifspacer"></div>
				<?php echo modGuTarifrechner2Helper::getCustomerSelect($params->get('kundengruppe3',1)); ?>
			</div>
			<div class="formrow2">
				<input type="text" name="verbrauch" value="" placeholder="<?php echo JText::_('MOD_GU_TARIFRECHNER_JAHRESVERBRAUCH'); ?>" maxlength="6" onkeyup="this.value=this.value.replace(/\D/, '');" />
				<div class="gu_tarifspacer"></div>
				<select name="laufzeit" size="1" data-placeholder="<?php echo JText::_('MOD_GU_TARIFRECHNER_VERTRAGSLAUFZEIT'); ?>">
					
					 <option value="0" selected="selected" style="display:none;"><?php echo JText::_('MOD_GU_TARIFRECHNER_VERTRAGSLAUFZEIT'); ?></option>
					
					<?php // Ausgabe dropdown
						for ($vl= $params->get('minlaufzeit3',6); $vl<=$params->get('maxlaufzeit3',36); $vl++)
						{
							echo '<option value="'.$vl.'">'.$vl.' '.JText::_('MOD_GU_TARIFRECHNER_MONATE').'</option>';
						}
					?>
				</select>
			</div>
			<div class="clear"></div>
		</div>
		<div class="formcol3">
			<input type="button" data-tariftype="3" class="gu_tarif_tarif3abfragen gu_tarif_tarifabfragen" value="<?php echo $params->get('calltoaction',JText::_('MOD_GU_TARIFRECHNER_CALLTOACTION_BUTTON')); ?>" />
			<div class="gu_tarifspacer"></div>
			<div class="gu_tarif_formmsg"></div>
		</div>
		<div class="clear"></div>
	</form>
	</div>
<?php endif;
// Ausgabe Formular Tarif 2
if ($params->get('tarif2online',0)): ?>
	<div class="tarif2" style="<?php if ($params->get('activetarif',1)!=2) { echo 'display: none;'; }; ?>">
		<form class="gu_tarif2form" autocomplete="off">
		<div class="form2col">
			<div class="formrow1">
				<input type="hidden" name="energieart" value="<?php echo $params->get('tarifart2',1); ?>" />
				<input type="hidden" name="activetarif" value="2" />
				<input type="text" name="tarifplz" placeholder="<?php echo JText::_('MOD_GU_TARIFRECHNER_PLZ'); ?>" maxlength="5" onkeyup="this.value=this.value.replace(/\D/, '');" />
				<div class="gu_tarifspacer"></div>
				<?php echo modGuTarifrechner2Helper::getCustomerSelect($params->get('kundengruppe2',1)); ?>
			</div>
			<div class="formrow2">
				<input type="text" name="verbrauch" placeholder="<?php echo JText::_('MOD_GU_TARIFRECHNER_JAHRESVERBRAUCH'); ?>" maxlength="6" onkeyup="this.value=this.value.replace(/\D/, '');" />
				<div class="gu_tarifspacer"></div>
				<select name="laufzeit" size="1" data-placeholder="<?php echo JText::_('MOD_GU_TARIFRECHNER_VERTRAGSLAUFZEIT'); ?>">
					 
					<option value="0" selected="selected" style="display:none;"><?php echo JText::_('MOD_GU_TARIFRECHNER_VERTRAGSLAUFZEIT'); ?></option> 
					
					<?php 
					
					// Ausgabe dropdown
						$min = $params->get('minlaufzeit2',6);
						$max = $params->get('maxlaufzeit2',36);
						/*
						$keys = range($min, $max);
						$values = array_map(function($v) {return $v . " " . JText::_('MOD_GU_TARIFRECHNER_MONATE');} , $keys);
						$values = array_combine($keys, $values);
						
						echo JHtml::_('select.options', $values);
						*/

						for ($i=$min; $i<=$max; $i++) : 
					?>
						<option value="<?php echo $i; ?>"><?php echo $i . " " . JText::_('MOD_GU_TARIFRECHNER_MONATE'); ?></option>
					<?php endfor; ?>
				</select>
			</div>
			<div class="clear"></div>
		</div>
		<div class="formcol3">
			<input type="button" data-tariftype="2" class="gu_tarif_tarif2abfragen gu_tarif_tarifabfragen" value="<?php echo $params->get('calltoaction',JText::_('MOD_GU_TARIFRECHNER_CALLTOACTION_BUTTON')); ?>" />
			<div class="gu_tarifspacer"></div>
			<div class="gu_tarif_formmsg"></div>
		</div>
		<div class="clear"></div>
		</form>
	</div>
<?php endif; ?>
</div>