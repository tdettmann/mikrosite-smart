<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_tags_popular
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

?>
<?php JLoader::register('TagsHelperRoute', JPATH_BASE . '/components/com_tags/helpers/route.php'); ?>
<?php if ($list) : ?>
<div class="tagssimilar<?php echo $moduleclass_sfx; ?>">
	<ul>
	<?php foreach ($list as $i => $item) : ?>
		<li>
			<?php $item->route = new JHelperRoute; 
			$titleparts = explode(' - ', htmlspecialchars($item->core_title),2); // Zerlegt den Titel in max. 2 Teile
			?>
			<a href="<?php echo JRoute::_(TagsHelperRoute::getItemRoute($item->content_item_id, $item->core_alias, $item->core_catid, $item->core_language, $item->type_alias, $item->router)); ?>">
				<span class="headline1"><?php echo $titleparts[0]; ?>:</span> <span class="headline2"><?php echo $titleparts[1]; ?></span>
			</a>
		</li>
	<?php endforeach; ?>
	</ul>
</div>
<?php else : ?>
	<span><?php 
	// echo JText::_('MOD_TAGS_SIMILAR_NO_MATCHING_TAGS'); 
	?></span>
<?php endif; ?>

