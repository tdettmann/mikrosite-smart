<?php
/**
 * @package     Joomla.Site
 * @subpackage  Layout
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Create a shortcut for params.
$params = $displayData->params;
$canEdit = $displayData->params->get('access-edit');
JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('behavior.framework');

$itemtitle  = $this->escape($displayData->title);
$titleparts = explode(' - ', $itemtitle,2); // Zerlegt den Titel in max. 2 Teile
$titlelink =JRoute::_(ContentHelperRoute::getArticleRoute($displayData->slug, $displayData->catid));
?>

	<?php if ($params->get('show_title') || $displayData->state == 0 || ($params->get('show_author') && !empty($displayData->author ))) : ?>
		<div class="page-header">

			<?php if ($params->get('show_title')) : ?>

					<?php if ($params->get('link_titles') && $params->get('access-view')) : ?>
						<h1><a href="<?php echo $titlelink; ?>"><?php echo $titleparts[0]; ?></a></h1>
						<h2><a href="<?php echo $titlelink; ?>"><?php echo $titleparts[1]; ?></a></h2>
					<?php else : ?>
						<h1><?php echo $titleparts[0]; ?></h1>
						<h2><?php echo $titleparts[1]; ?></h2>
					<?php endif; ?>

			<?php endif; ?>

			<?php if ($displayData->state == 0) : ?>
				<span class="label label-warning"><?php echo JText::_('JUNPUBLISHED'); ?></span>
			<?php endif; ?>
		</div>
	<?php endif; ?>
