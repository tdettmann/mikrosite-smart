<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_contact
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$cparams = JComponentHelper::getParams('com_media');

?>
<div class="contact<?php echo $this->pageclass_sfx?>">
	<?php if ($this->params->get('show_page_heading')) : ?>
		<h1>
			<?php echo $this->escape($this->params->get('page_heading')); ?>
		</h1>
	<?php endif; ?>
	<?php if ($this->params->get('show_contact_category') == 'show_no_link') : ?>
		<h2>
			<span class="contact-category"><?php echo $this->contact->category_title; ?></span>
		</h2>
	<?php endif; ?>
	<?php if ($this->params->get('show_contact_category') == 'show_with_link') : ?>
		<?php $contactLink = ContactHelperRoute::getCategoryRoute($this->contact->catid); ?>
		<h2>
			<span class="contact-category"><a href="<?php echo $contactLink; ?>">
				<?php echo $this->escape($this->contact->category_title); ?></a>
			</span>
		</h2>
	<?php endif; ?>
<div class="contact-content">
	<?php if ($this->contact->image && $this->params->get('show_image')) : ?>
		<div class="item-image thumbnail pull-right">
			<?php echo JHtml::_('image', $this->contact->image, JText::_('COM_CONTACT_IMAGE_DETAILS'), array('align' => 'middle')); ?>
		</div>
	<?php endif; ?>
	<?php if ($this->contact->name && $this->params->get('show_name')) : ?>
			<h3>
				<?php if ($this->item->published == 0) : ?>
					<span class="label label-warning"><?php echo JText::_('JUNPUBLISHED'); ?></span>
				<?php endif; ?>
				<span class="contact-name"><?php echo $this->contact->name; ?></span>
			</h3>
	<?php endif;  ?>
	
	<?php if ($this->contact->con_position && $this->params->get('show_position')) : ?>
		<div class="contact-position">
			<?php echo $this->contact->con_position; ?>
		</div>
	<?php endif; ?>
	
	<div class="contact-telefon-mail">

		<?php if ($this->contact->telephone && $this->params->get('show_telephone')) : ?>
			<span class="contact-telephone">
				Telefon: <?php echo nl2br($this->contact->telephone); ?><br />
			</span>
		<?php endif; ?>
		
		<?php if ($this->contact->fax && $this->params->get('show_fax')) : ?>
			<span class="contact-fax">
				<?php echo nl2br($this->contact->fax); ?><br />
			</span>
		<?php endif; ?>
		
		<?php if ($this->contact->mobile && $this->params->get('show_mobile')) :?>
			<span class="contact-mobile">
				<?php echo nl2br($this->contact->mobile); ?><br />
			</span>
		<?php endif; ?>
		
		<?php if ($this->contact->webpage && $this->params->get('show_webpage')) : ?>
			<span class="contact-webpage">
				<a href="<?php echo $this->contact->webpage; ?>" target="_blank">
				<?php echo $this->contact->webpage; ?></a><br />
			</span>
		<?php endif; ?>
		
		<?php if ($this->contact->email_to && $this->params->get('show_email')) : ?>
			<span class="contact-emailto">
				<?php echo $this->contact->email_to; ?><br />
			</span>
		<?php endif; ?>
		
		<?php if ($this->params->get('allow_vcard')) :	?>
			<?php // echo JText::_('COM_CONTACT_DOWNLOAD_INFORMATION_AS');
			?>
			<a class="downloadvcard" href="<?php echo JRoute::_('index.php?option=com_contact&amp;view=contact&amp;id='.$this->contact->id . '&amp;format=vcf'); ?>">
			<?php echo JText::_('COM_CONTACT_VCARD');?></a>
		<?php endif; ?>
	</div>
	<h4>Anschrift</h4>
	<div class="contact-address">
		<?php if (($this->params->get('address_check') > 0) &&
			($this->contact->address || $this->contact->suburb  || $this->contact->state || $this->contact->country || $this->contact->postcode)) : ?>
		
			<?php if ($this->contact->address && $this->params->get('show_street_address')) : ?>
				<span class="contact-street">
					<?php echo nl2br($this->contact->address); ?>
				</span>
			<?php endif; ?>
			<?php if ($this->contact->postcode && $this->params->get('show_postcode')) : ?>
				<span class="contact-postcode">
					<?php echo $this->contact->postcode .' '; ?>
				</span>
			<?php endif; ?>
			<?php if ($this->contact->suburb && $this->params->get('show_suburb')) : ?>
				<span class="contact-suburb">
					<?php echo $this->contact->suburb .'<br />'; ?>
				</span>
			<?php endif; ?>
			<?php if ($this->contact->state && $this->params->get('show_state')) : ?>
				<span class="contact-state">
					<?php echo $this->contact->state . '<br />'; ?>
				</span>
			<?php endif; ?>
			
			<?php if ($this->contact->country && $this->params->get('show_country')) : ?>
				<span class="contact-country">
					<?php echo $this->contact->country .'<br />'; ?>
				</span>
			<?php endif; ?>
		<?php endif; ?>
	</div>
</div>
	<?php if ($this->params->get('show_email_form') && ($this->contact->email_to || $this->contact->user_id)) : ?>
		<h3><?php echo JText::_('COM_CONTACT_EMAIL_FORM');  ?></h3>

		<?php echo $this->loadTemplate('form');  ?>

	<?php endif; ?>

	<?php if ($this->params->get('show_links')) : ?>
		<?php echo $this->loadTemplate('links'); ?>
	<?php endif; ?>
</div>
