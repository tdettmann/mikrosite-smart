<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_contact
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
JHtml::_('behavior.framework');

$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));

// Create a shortcut for params.
$params = &$this->item->params;
?>
<?php if (empty($this->items)) : ?>
	<p> <?php echo JText::_('COM_CONTACT_NO_CONTACTS'); ?>	 </p>
<?php else : ?>
<ul class="category list-striped">	
<?php foreach ($this->items as $i => $item) : ?>
	<li class="<?php echo ($i % 2) ? "odd" : "even"; ?> contact">
		<?php if ($item->image && $this->params->get('show_image')) : ?>
		<div class="item-image">
			<a href="<?php echo JRoute::_(ContactHelperRoute::getContactRoute($item->slug, $item->catid)); ?>">
				<?php echo JHtml::_('image', $item->image, JText::_('COM_CONTACT_IMAGE_DETAILS'), array('align' => 'middle')); ?>
			</a>
		</div>
		<?php endif; ?>
		<h3 class="item-title">
			<?php if ($this->items[$i]->published == 0) : ?>
				<span class="label label-warning"><?php echo JText::_('JUNPUBLISHED'); ?></span>
			<?php endif; ?>
			<a href="<?php echo JRoute::_(ContactHelperRoute::getContactRoute($item->slug, $item->catid)); ?>">
				<?php echo $item->name; ?></a>
		</h3>
		<div class="contact-data">
		<?php if ($this->params->get('show_position_headings')) : ?>
			<span class="item-position">
				<?php echo $item->con_position; ?><br />
			</span>
		<?php endif; ?>

		<?php if ($this->params->get('show_email_headings')) : ?>
			<span class="item-email">
				<?php echo $item->email_to; ?><br />
			</span>
		<?php endif; ?>

		<?php if ($this->params->get('show_telephone_headings')) : ?>
			<span class="item-phone">
				Telefon: <?php echo $item->telephone; ?><br />
			</span>
		<?php endif; ?>

		<?php if ($this->params->get('show_mobile_headings')) : ?>
			<span class="item-phone">
				Mobil: <?php echo $item->mobile; ?><br />
			</span>
		<?php endif; ?>

		<?php if ($this->params->get('show_fax_headings')) : ?>
		<span class="item-phone">
			Fax: <?php echo $item->fax; ?><br />
		</span>
		<?php endif; ?>

		<?php if ($this->params->get('show_suburb_headings')) : ?>
		<span class="item-suburb">
			<?php echo $item->postcode; ?> <?php echo $item->suburb; ?><br />
		</span>
		<?php endif; ?>

		<?php if ($this->params->get('show_state_headings')) : ?>
		<span class="item-state">
			<?php echo $item->state; ?><br />
		</span>
		<?php endif; ?>

		<?php if ($this->params->get('show_country_headings')) : ?>
		<span class="item-state">
			<?php echo $item->country; ?>
		</span>
		<?php endif; ?>
			
		<?php if ($this->params->get('allow_vcard')) :	?>
		<span class="item-vcard">
			<?php // echo JText::_('COM_CONTACT_DOWNLOAD_INFORMATION_AS');
			?>
			<a class="downloadvcard" href="<?php echo JRoute::_('index.php?option=com_contact&amp;view=contact&amp;id='.$item->id . '&amp;format=vcf'); ?>">
			<?php echo JText::_('COM_CONTACT_VCARD');?></a>
		</span>
		<?php endif; ?>
		</div>
	</li>
<?php endforeach; ?>
</ul>
<?php endif; ?>
