<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mailto
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div id="mailtosend-window">
	<h2>
	<?php echo JText::_('COM_MAILTO_EMAIL_SENT'); ?>
	</h2>
	<div class="maito_close">
		<a class="button" href="javascript:window.parent.jQuery('#interaktion_email').click();">
			Ok
		</a>
	</div>
</div>
