<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mailto
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
JHtml::_('behavior.keepalive');

$data	= $this->get('data');
// Defaultwerte setzen die im Formular angezeigt werden

if ($this->escape($data->mailto)){
    $mailto = $this->escape($data->mailto);
} else {
    $mailto = JText::_('COM_MAILTO_EMAIL_TO');
}

if ($this->escape($data->sender)){
    $sender = $this->escape($data->sender);
} else {
    $sender = JText::_('COM_MAILTO_SENDER');
}

if ($this->escape($data->from)){
    $from = $this->escape($data->from);
} else {
    $from = JText::_('COM_MAILTO_YOUR_EMAIL');
}
if ($this->escape($data->subject)){
    $subject = $this->escape($data->subject);
} else {
    $subject = JText::_('COM_MAILTO_SUBJECT');
}
?>
<script type="text/javascript">
    function mailtoValidateEmail(email){
	var emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
	var valid = emailReg.test(email);
	if(!valid) {
	    return false;
	} else {
	    return true;
	}
    }
	Joomla.submitbutton = function(pressbutton)
	{
		var form = document.getElementById('mailtoForm');

		// do field validation
		if (!mailtoValidateEmail(form.mailto.value) || !mailtoValidateEmail(form.from.value))
		{
			alert('<?php echo JText::_('COM_MAILTO_EMAIL_ERR_NOINFO'); ?>');
			return false;
		}
		// Default-Werte löschen
		if (form.mailto.value==jQuery("#sender_field").data("default"))
		{
		    form.mailto.value='';
		}
		if (form.subject.value==jQuery("#subject_field").data("default"))
		{
		    form.subject.value='';
		} 
		form.submit();
	}
</script>

<div class="mailto-window">
    <p>
	<?php echo JText::_('COM_MAILTO_EMAIL_TO_A_FRIEND'); ?>
    </p>

	<form action="<?php echo JUri::base() ?>index.php" id="mailtoForm" method="post">
		<div class="formelm">
<!--			<label for="mailto_field"><?php echo JText::_('COM_MAILTO_EMAIL_TO'); ?></label>-->
			<input type="text" id="mailto_field" name="mailto" class="inputbox" size="25" title="<?php echo JText::_('COM_MAILTO_EMAIL_TO'); ?>" value="<?php echo $mailto; ?>"/>
		</div>
		<div class="formelm">
<!--			<label for="sender_field"><?php echo JText::_('COM_MAILTO_SENDER'); ?></label>-->
			<input type="text" id="sender_field" name="sender" class="inputbox" title="<?php echo JText::_('COM_MAILTO_SENDER'); ?>" value="<?php echo $sender; ?>" size="25" />
		</div>
		<div class="formelm">
<!--			<label for="from_field"><?php echo JText::_('COM_MAILTO_YOUR_EMAIL'); ?></label>-->
			<input type="text" id="from_field" name="from" class="inputbox" title="<?php echo JText::_('COM_MAILTO_YOUR_EMAIL'); ?>" value="<?php echo $from; ?>" size="25" />
		</div>
		<div class="formelm">
<!--			<label for="subject_field"><?php echo JText::_('COM_MAILTO_SUBJECT'); ?></label>-->
			<input type="text" id="subject_field" name="subject" class="inputbox" title="<?php echo JText::_('COM_MAILTO_SUBJECT'); ?>" value="<?php echo $subject; ?>" size="25" on/>
		</div>
		<p>
			<button class="button" onclick="return Joomla.submitbutton('send');">
				<?php echo JText::_('COM_MAILTO_SEND'); ?>
			</button>
			<button class="button" onclick="window.parent.jQuery('#interaktion_email').click(); return false;">
			<?php echo JText::_('COM_MAILTO_CANCEL'); ?>
			</button>
		</p>
		<input type="hidden" name="layout" value="<?php echo $this->getLayout();?>" />
		<input type="hidden" name="option" value="com_mailto" />
		<input type="hidden" name="task" value="send" />
		<input type="hidden" name="tmpl" value="mailto" />
		<input type="hidden" name="link" value="<?php echo $data->link; ?>" />
		<?php echo JHtml::_('form.token'); ?>

	</form>
</div>
<script type="text/javascript">
jQuery(document).ready(function()
{
	// Defaultwerte loeschen/setzen wenn kein Wert eingegeben wurde
	// Ueber die Klasse defaultTextActive kann eine alternative Formatierung gesetzt werden
	jQuery('.inputbox').on('focus', function(){
		if (jQuery(this).val() == jQuery(this)[0].title)
		{
			jQuery(this).removeClass("defaultTextActive");
			jQuery(this).val("");
		}
	});
	jQuery('.inputbox').on('blur', function(){
		if (jQuery(this).val() == "")
			{
				jQuery(this).addClass("defaultTextActive");
				jQuery(this).val(jQuery(this)[0].title);
			}
	});
	// Weisst allen feldern die CSS-Klasse defaultTextActive falls Value == Wert Titel-Attribut 
	jQuery(".inputbox").blur();        
});
</script>