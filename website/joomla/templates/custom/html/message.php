<?php
defined('_JEXEC') or die;

function renderMessage($msgList)
{
	$buffer  = null;
	$alert = array('error' => 'alert-error', 'warning' => '', 'notice' => 'alert-info', 'message' => 'alert-success');

	// Only render the message list and the close button if $msgList has items
	if (is_array($msgList) && (count($msgList) >= 1))
	{
		$buffer .= '<div id="cms-message" class="opacity-wrapper">';
		$buffer .= '<div class="opacity"></div>';
		$buffer .= '<div class="page-center">';
		$buffer .= "\n<div id=\"system-message-container\">";
		foreach ($msgList as $type => $msgs)
		{
			$buffer .= '<div class="alert ' . $alert[$type]. '">';
			$buffer .= "\n<span class=\"alert-heading\">" . JText::_($type) . ":</span>";
			if (count($msgs))
			{
				foreach ($msgs as $msg)
				{
					$buffer .= "\n\t\t" . $msg . "<br />";
				}
			}
			$buffer .= "\n</div>"; // alert
		}
		$buffer .= '<a class="close" data-dismiss="alert">' . JText::_('COM_CONTENT_CLOSE_HINT') . '</a>';
		$buffer .= '<div class="clear"></div>';
		$buffer .= "\n</div>"; // system-message-container
		$buffer .= '</div>'; //page-inner
		$buffer .= '</div>'; // cms-message
	}
	return $buffer;
}