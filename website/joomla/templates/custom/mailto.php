<?php
/**
 * Joomla-Template für die Stadtwerke Pforzheim.
 * Mailto-Seite im Iframe
 * 
 * @version	1.0
 * @package	Joomla.Site
 * @subpackage	swp
 * @author      GU KOMMUNIKATION Karlsruhe
 * @copyright	Copyright (C) 2015 Gordana Uzelac, Agentur fÃ¼r Kommunikation - GU KOMMUNIKATION, Karlsruhe.
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// Initialisierung
$doc = JFactory::getDocument();

// Add JavaScript Frameworks
JHtml::_('behavior.framework');

// CSS-Dateien hinzufügen
$doc->addStyleSheet($this->baseurl . '/templates/' . $this->template . '/css/template.css', 'text/css', 'all');

// Javascript
$doc->addScript($this->baseurl . '/media/jui/js/jquery.min.js');
$doc->addScript($this->baseurl . '/media/jui/js/jquery-noconflict.js');
?>
<!DOCTYPE html>
<html xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>">
    <head>
	<meta charset="utf-8">
	<jdoc:include type="head" />
    </head>
    <body class="mailto-template">
	<jdoc:include type="message" />
	<div id="component"><jdoc:include type="component" /></div>
	<script type="text/javascript">
		jQuery(document).ready(function(){
			jQuery('a.close').on('click', function() {
				jQuery('#cms-message').hide();
				return false;
			});
		});
	</script>
    </body>
</html>