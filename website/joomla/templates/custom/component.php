<?php
/**
 * Joomla-Template für die Stadtwerke Pforzheim.
 * Componenten View
 * 
 * @version	1.0
 * @package	Joomla.Site
 * @subpackage	swpforzheim
 * @author      GU KOMMUNIKATION Karlsruhe
 * @copyright	Copyright (C) 2013 Gordana Uzelac, Agentur für Kommunikation - GU KOMMUNIKATION, Karlsruhe.
 * @todo        Code vollständig dokumentieren, print.css hinzufügen
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

// Initialisierung
include_once JPATH_THEMES.'/'.$this->template.'/includes/template_logic.php';

// Welches Komponenten-Layout soll verwendet werden?
$simpellayout=true; // Default: Einfaches Layout verwenden
// Ermitteln der Komponente und des Views
jimport( 'joomla.environment.request' );
$component_name=JRequest::getVar('option', '');

if ($component_name=='com_acymailing'){
	// Newsletter Weiterleitungs Funktion
	if ((JRequest::getVar('ctrl', '')==archive) && (JRequest::getVar('task', '')=='forward')){
		$simpellayout=false; // True: Website Layout verwenden
	}
}

// Layouts laden
if ($simpellayout):
	//Einfaches Layout laden. Z.B. Bei Darstellung in Iframes
	include_once JPATH_THEMES.'/'.$this->template.'/includes/template_component_view.php';
else: 
	// Anfang Website-Layout ---------------------------------------------------
?>
<html xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>">
<head>
	<?php 
	// Inhalt des Head laden
	include_once JPATH_THEMES.'/'.$this->template.'/includes/template_head.php'; 
	?>
</head>
<body id="swptemplate" class="<?php echo $activmenuclass; ?><?php echo (constant('ISMOBIL') ? ' mobil' : ''); ?><?php echo ($isBoxlayout && !constant('ISMOBIL') ? ' largeheader' : ''); ?>">
    <div id="page-wrapper">
		<div id="page" class="container-fluid">
			<?php 
			// Kopf-Bereich laden
			include_once JPATH_THEMES.'/'.$this->template.'/includes/template_page_header.php';
			
			// Content-Bereich für speziele Komponenten-Ansichten laden
			include_once JPATH_THEMES.'/'.$this->template.'/includes/template_page_component.php';
			
			// Fuss-Bereich laden
			include_once JPATH_THEMES.'/'.$this->template.'/includes/template_page_footer.php';
			?>
		</div>
	</div>
	<jdoc:include type="message" />
	<div id="bookmarkhint" class="opacity-wrapper">
		<div class="opacity"></div>
		<div class="page-inner">
			<?php echo ($isMobile ? $this->params->get('bookmarkhintmobil') : $this->params->get('bookmarkhint')); ?>
			<a class="close" onclick="jQuery('#bookmarkhint').toggle();" href="#bookmark"><?php echo $this->params->get('bookmarkclosetitle'); ?></a>
			<div class="clear"></div>
		</div>
    </div>
	<?php
	// Template Javascripts laden
	include_once JPATH_THEMES.'/'.$this->template.'/includes/template_footer_js.php';
	?>
</body>
</html>
<?php // Ende Website-Layout ---------------------------------------------------
endif;
