<?php

class WFQuotationmarksPluginConfig {

    public static function getConfig(&$settings) {
        $wf = WFEditor::getInstance();

        $settings['quotation'] = $wf->getParam('quotationmarks.quotation', 'de');
        $settings['encoding'] = $wf->getParam('quotationmarks.encoding', 0);

    }
}

?>