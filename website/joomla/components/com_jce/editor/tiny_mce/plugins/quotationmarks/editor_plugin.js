(function() {
	tinymce.PluginManager.requireLangPack('quotationmarks');
	   
	tinymce.create('tinymce.plugins.Quotationmarks', {
		init : function(ed, url) {
			
			var q = ed.getParam('quotation');
			
			if (!q) 
				return false;
			
			var t = this;
			
			var symbols = {
				bd: ['„',  '&bdquo;',  '&#8222;'],
				ld: ['“',  '&ldquo;',  '&#8220;'],
				rd: ['”',  '&rdquo',   '&#8221;'],
				                        
				la: ['«',  '&laquo;',  '&#171;'],
				ra: ['»',  '&raquo;',  '&#187;'],
				                        
				ls: ['‘',  '&lsquo;',  '&#8216;'],
				rs: ['’',  '&rsquo;',  '&#8217;'],
				sb: ['‚',  '&sbquo;',  '&#8218;'],
				
				qt: ['"',  '&quot;',   '&#34;']
			};
			
			var quotations = { 
				de:			{ left: 'bd', right: 'ld' },
				fr: 		{ left: 'la', right: 'ra' },
				en_double:	{ left: 'ld', right: 'rd' },
				en_single:	{ left: 'ls', right: 'rs' },
				en_single2:	{ left: 'sb', right: 'ls' },
				typewriter:	{ left: 'qt', right: 'qt' }
			};
			
			
			// Register buttons
			ed.addButton('quotationmarks', {
				title 	: 'quotationmarks.desc', 
				cmd 	: 'mceQuotationmarks', 
				image 	: url + '/img/quotationmarks.png'
			});
			 
			// Register commands
			ed.addCommand('mceQuotationmarks', function() {
				var html = ed.selection.getContent();
				
				var q 			= ed.getParam('quotation');
				var encoding 	= ed.getParam('encoding');
				
				var r = symbols[quotations[q].right][encoding];
				var l = symbols[quotations[q].left][encoding];
				
				// Insert Qutationmarks
				var regexp 		= /(\s*)([^\s].*[^\s])(\s*)/;
				html = html.replace(regexp, '$1' + l + '$2' + r + '$3');
				
				ed.execCommand("mceInsertContent", false, html);
				return true;
			});
		}
	});
	
	// Register plugin
	tinymce.PluginManager.add('quotationmarks', tinymce.plugins.Quotationmarks);
})();